---
layout: math
title: The Axioms of ZFC
---

## The Axioms of ZFC

Now that we have covered the basics of predicate calculus, we can finally begin to talk about ZFC in a meaningful way.

ZFC takes predicate calculus with identity and adds one additional symbol, the
membership relation symbol $\in$.  For simplicity's sake we'll also add
$\emptyset$ as a symbol (though we'll see later that this is not technically
necessary).

So let's look at the simplest axiom

### The Axiom of Zero

$$
\exists x \forall y \neg(y \in x)
$$

"There exists some set  $x$, such that forall $y$, $y$ is not a member of $x$"

If no set is a member of $x$, then $x$ must be empty.  Therefore the axiom of zero states that "The empty set exists".

This is perhaps the most intuitive axiom of the whole bunch.  But nevertheless,
we need to take it as an axiom since we cannot prove (without making other
assumptions) that it does exist.

### The Axiom of Extensionality

$$
\forall x \forall y (x = y \iff \forall z (z \in x \iff z \in y))
$$

"For all sets $x$ and $y$, they are equal if and only if for any set $z$, if $z
\in x$, then $z \in y$ and vice-versa."

This axiom is essentially defining what it means for two sets to be equal.
Again this axiom is pretty intuitive, but it does have a few important
consequences.  For one thing, it asserts that there are not different "types"
or "flavors" of sets.  For example, imagine if we had a concept of colors of
sets.  We could have two sets that contain the same elements, but one set was
red and another blue.  Thus we'd have two sets that contained the same elements
but were still not considered equal.  The axiom of extensionality assures that
this can never be the case.

A corrolary of extensionality is that there is exactly one empty set.  This is
why it's safe to unambiguously refer to _the_ empty set and give it the unique
symbol $\emptyset$.

### The Axiom of Pairing

$$
\forall x \forall y \exists z (x \in z \land y \in z \land \forall w (w \in z \to w = x \lor w = y))
$$

"For all sets $x$ and $y$, there exists a set $z$ such that $x$ and $y$ are the only members of $z$."

In other words, given any two sets $x$, and $y$, we can create $\\{x, y\\}$.

Together, the axioms of zero and pairing ensure that an infinite number of sets
exist.  Since we know that $\emptyset$ exists, and from pairing we know that
$\\{\emptyset, \emptyset\\} = \\{\emptyset\\}$ exists, and so on.  This
sequence of sets was Zermelo's first attempt at trying to encode the natural
numbers as sets, with $0 = \emptyset, 1 = \\{\emptyset\\}, \dots$, although
we'll see that another encoding has since become the defacto.

#### Ordered Pairs

Notice that a pair $\\{a,b\\}$ is still just a set which is unordered.  So
$\\{a,b\\} = \\{b,a\\}$.  But in many cases it is extremely useful to have a
concept of an ordered pair $(a,b)$ where $(a,b) \neq (b,a)$.  This means that
we need to define a set that somehow encodes the idea that it has a "first"
element and a "second" element.  In particular, it should satisfy the
statement (which we can refer to as the ordered-pair axiom):

$$
(a,b) = (c,d) \iff a = c \land b = d
$$

There are several ways to achieve such an encoding, but the most common is known as the Kuratowski pair: 

$$
(a,b) = \{\{a\}, \{a,b\}\}
$$

Now we can define the "first element" as the set that is in both subsets, and
the other element is the "second" one.  More importantly we can easily see that
the ordered pair axiom holds.

It is almost never the case that we actually refer to an ordered pair via its
Kuratowski encoding, but it is important that there indeed is a way to encode
the notion of an ordered pair as a regular unordered set.  We'll see later on
that ordered pairs are extremely useful in defining [functions and
relations](functions) on sets.

### The Axiom of Union

$$
\forall x \exists y \forall z (z \in x \to \forall w (w \in z \to w \in y))
$$

"Given a set of sets $x$, there is a set $y$ such that every element of every set in $x$ is in $y$."

This is the first place where our "true" definition differs somewhat from the
simplified definition presented earlier.  The simple definition only applied to
two sets and created $A \cup B$.  This version takes a set of sets and
"flattens" it into a new set that contains all the elements of all the sets.

For example, if we have a set:

$$
A = \{ \{a, b\} , \{c, d, e\}, \emptyset \}
$$

then the union of all the sets of $A$, denoted $\bigcup A$ is:

$$
\bigcup A = \{a, b, c, d, e\}
$$

### The Axiom of Powerset

$$
\forall x \exists y \forall z (z \subset x \iff z \in y)
$$

"For every set $x$, there is a set $y$ such that every possible subset of $x$
is an element of $y$"

For a set $x$, the $y$ in the statement is called the power set of $x$, and we
use the notation $\powerset{x}$.  Let's look at a simple example.  Consider the set $A = \\{a, b, c\\}$ of 3 elements.  Let's enumerate all the possible subsets of $A$:

$$
\begin{array}{c, c, c}

\emptyset \\
\{a\} & \{b\} & \{c\} \\
\{a, b\} & \{a, c\}, & \{b, c\} \\
\{a, b, c\}
\end{array}
$$

$\powerset{A}$ is just the set of all these set, $\\{ \emptyset, \\{a\\}, \\{b\\}, \\{c\\}, \\{a, b\\}, \dots \\}$.

One important fact about the powerset, and one reason why it's included as an
axiom, is that if a set $A$ has $n$ elements, then $\powerset{A}$ has $2^n$
elements.  The next axiom may add some confusion around that fact, but we'll
see soon that everything works out.

### The Axiom of Infinity

All of the previously mentioned axioms are more-or-less the basic ones.  They
are important but generally only because nothing would make sense without them.
The next axioms, starting with infinity, each give ZFC significantly more
power.

The axiom of infinity is:

$$
\exists x ~(\emptyset \in x \land \forall y (y \in x \to y \cup \{y\} \in x))
$$

This axiom states that there exists a set $x$ that contains $\emptyset$, and
for every member $y$ of $x$, $x$ also contains the set $y \cup \\{y\\}$.  This
$x$ must be infinite in size.

Before we look at this set in more depth, let's explore a little bit about why
we need to explicitly state that such a set exists as an axiom.  We showed that
with the above axioms, we're able to construct an infinite number of sets.  If
we have some set $x$, then we know we can create $\\{x\\}, \\{\\{x\\}\\},
\\{\\{\\{x\\}\\}\\}$, and so on.  So we do not need the axiom of infinity to
prove that an infinite number of sets exist.  Instead, we need it to prove that
a single set with an infinite number of members exists.  All of the sets
produced from applying the axioms of union and pairing only have a finite
number of elements.

It will probably not seem obvious why this infinite set is defined this way.
Why require that for a $y$, $x$ contains $y \cup \\{y\\}$ and not some other
set like $\\{y\\}$?  We'll explore this much more in depth in the section on
[ordinals](ordinals), but the short answer is that this set is actually the set
of natural numbers encoded as sets, and that the particular form of encoding
used in this axiom has several important benefits.


_As an aside, the axiom of infinity doesn't actually state that $\omega$ itself
specifically exists, since the set it describes could contain sets that aren't
ordinals.  But this is rectified with the next few axioms_

### The Axiom Schema of Separation

Before we state it in full, notice that we referred to Separation as an axiom
schema.  This is because it is actuallly an infinite set of similar axioms.
Let's restate that Separation says that given a set and any formula, we can
create a set that only contains the elements of $A$ that satisfy the formula.
The problem here is that predicate calculus can only quantify over sets, not
formulas.  To state Separation as a single axiom, there would need to something
like $\forall x \forall \phi \dots$, but this is not a valid sentence in the
language of predicate calculus.

Therefore, Separation is an axiom schema, in that a version of it exists for
every possible formula.  Thus for some given formula $\phi$, the separation
axiom for that formula is:

$$
\forall x \exists y \forall z ~(z \in x \land \phi(z) \iff z \in y)
$$

Let's look at a few examples

Suppose $\phi(x) = x = \emptyset \lor x = \\{\emptyset\\}$.  Now suppose we are given two sets $A_1 = \\{\emptyset\\}$, $A_2 = \\{\\{\\{\emptyset\\}\\}\\}$.  Separation says we can form the sets:

$$
\begin{array}{lll}
B_1 & = & \{a : a \in A_1 \land \phi(a)\} = \{\emptyset\} \\
B_2 & = & \{a : a \in A_2 \land \phi(a)\} = \emptyset\\
\end{array}
$$

Thus separation is a tool to pull out elements of a set with some desired property and create a new set out of them.

#### Russell's Paradox

Notice that Separation requires that we must have some given set $S$ to which
we apply $\phi$.  In other words, Separation does _not_ allow us to simply
create a set of everything that satisfies $\phi$.  So while we can create "The
subset of all elements of some set $S$ that are even numbers", seperation does
not allow us to create "the set of all even numbers".  This restriction is important because without it, we hit a logical paradox first discovered by Bertrand Russell in the early 1900's.

Early version of Separation did not have that restriction, so given some $\phi$, we could freely create $\\{x : \phi(x)\\}$.  However Russell noted that for some formulas, this resulted in a paradox.  Consider the formula

$$
\phi(x) : x \notin x
$$

This is a simple formula that is true for any set $x$ that does not contain
itself (be aware that none of the axioms stated so far allow us to create a set
that contains itself).  Clearly there are many sets that satisfy this formula.  The old axiom of Separation thus says we can create the set

$$
X = \{x : \phi(x)\} = \{x : x \notin x\}
$$

So $X$ is the set of all sets that do not contain themself.  But $X$ is itself
a set, so does $X$ contain itself?  If $X$ does not contain itself, that would
mean $X \notin X$ and $\phi(X)$ is true.  But that would mean that by its
definition, $X$ would be a member of $X$.  And if we assume that $X \in X$,
then $\phi(X)$ is false and thus $X \notin X$.  This is a contradiction, $X \in
X \iff X \notin X$.  The only resolution for this paradox is that $X$ itself
cannot exist in the first place.  Thus the Axiom of Separation was modified to
disallow the construction of paradoxical sets like $X$. 

In an intuive sense, though, it seems like we should be able to somehow collect
together all the sets that do not contain themselves but still avoid the
paradox.  The way set theorists get around the issue is to say that $X$ does
exist, but it is not a set.  More accurately we could say that $X$ is a set,
but not a set _definable_ in ZFC.  So while we can talk about a set like $X$ in
everyday life, we will not be able to speak about it in defintions or theorems
of ZFC.  Normally we refer to this type of non-set as a **class**.  Classes are
technically not a part of ZFC, but we mention them informally to capture ideas
that are generally not expressable in ZFC.

This goes to show that ZFC is not an attempt to describe every possible
collection, only those which behave the rules laid out in the axioms.  There
are, in fact, other formulations of set theory that do have an in-house notion
of classes.  One such theory, Von-Nemann-Godel-Bernays set theory (NBG for
short) has a distinction between classes and sets.  However it turns out that
adding classes does not make NBG much stronger than ZFC, as it has been proven
that every theorem provable in one theory is equivalent to a provable theorem
of the other.

### The Axiom Schema of Replacement

Like Separation, Replacement is an axiom schema, so there are in fact an
infinite number of similarly structured axioms, one for each formula.  However,
unlike separation, which places no restriction on the formulas themselves,
Replacement requires that formulas be "function-like".  This means that the
formula should be of the form $\phi(x,y)$ with 2 free variables, and that for
every $x$ there is a unique $y$ such that $\phi(x,y)$ is true.  So $\phi$ is
not actually a function, it is still just a formula, but we can in a way
"think" of it like a function.

For example consider the formula:

$$
\phi(x, y) : y = x
$$

Obviously given any $x$, there is exactly only one possible value for $y$, namely $x$ itself.  So $\phi$ qualifies as a function-like formula.  However a formula

$$
\phi(x, y) : \forall z (z \in x \to z \in y)
$$

is not function-like, since for any $x$, any superset of $x$ would be an
acceptable value of $y$, so not every $x$ has a _unique_ $y$.  Likewise, the formula

$$
\phi(x, y) : y = \emptyset \land y \neq x
$$

is not function-like, since there is no suitable $y$ when $x$ is $\emptyset$
(despite the fact that $\emptyset$ is a suitable $y$ for every other set).

So now, given our definition of a function-like formula, the **Axiom Schema of Replacement** is, for every such formula $\phi(x,y)$:

$$
\exists S \forall A \forall x ( x \in A \land \to \exists y ~ \phi(x,y) \land y \in S)
$$

So given a set $A$, there is a set $S$ such that for each $x \in A$, $f(x) \in
S$.  Notice that the axiom doesn't necessarily state that $S$ only contains the
range of $A$.  It could contain other elements as well, but then we can use
Separation to "prune" all of the non-range elements out of $S$ to create a set
$T$ that contains exactly the range of $\phi$ on $A$.

How can we state in predicate calculus that $\phi(x,y)$ is function-like?

$$
\forall x \exists y (\phi(x,y) \land \forall z (\phi(x,z) \to y = z))
$$

### The Axiom of Foundation

$$
\forall x (x \neq \emptyset \to \exists y (y \in x \land y \cap x = \emptyset))
$$

The axiom state for any non-empty set $x$, it contains at least one member $y$ such that $x$ and $y$ have no elements in common.  This axiom might sound a little arbitrary at first, but it encompasses two important restrictions on sets.

#### No set can be an element of itself

To see how foundation enforces this, let's consider some set $A$ that is a
member of itself.  That means $A$ can be written as $\\{A, x_1, x_2, \dots \\}$
that contains $A$ as well as possibly other elements.  Foundation says that
there must be at least one element $x$ of $A$ such that $x$ and $A$ have no elements in common, ie. $x \cap A = \emptyset$.  Now so far this seems fine.  After all if we have a set like $A = \\{A, \emptyset\\}$ then if we choose $\emptyset$ as the $x$, then $A \cap \emptyset = \emptyset$.  So a set containing itself doesn't actually violate the axiom.

But now consider $B = \\{A\\}$, which is guaranteed to exist from the pairing
axiom.  In order for $B$ to satisfy the axiom, there must be an $x \in B$ such
that $x \cap B = \emptyset$.  But the only element of $B$ is $A$ and $B \cap A
= A$.  So It is not $A$ that violates the axiom, but $\\{A\\}$.  And since this
applies to any set that contains itself, it must be the case that a set cannot
contain itself if we want it to adhere to the axiom of Foundation.

#### There can be no infinite decreasing sequence of sets

By this we mean we cannot have $x_1 \in x_2, x_2 \in x_3, x_3 \in x_4, \dots$
ad infinitum.  If we have some deeply nested set of sets of sets etc., that
nesting must always be finite, such that a set cannot be infinitely "deep."


### The Axiom of Choice

$$
\forall A \exists B \forall c ~(c \in A \to \exists ! d ~(d \in c \land d \in B))
$$

This axiom states that given a set of sets $A$, there is a set $B$ such that $B$ contains exactly one element of each member of $A$.  For example, suppose $A$ is the set:

$$
\big\{ \{1, 2\}, \{3, 4\}, \{5, 6\} \big\}
$$

Then a valid choice for $B$ could be $\\{1, 3, 5\\}$.  Now for finite sets AC
is not needed to create sets like $B$, but when $A$ is infinite there are
situations when it is needed.  In particular, AC allows us to create a choice
set without specifically saying which element we choose out of each set.  This
is notably different from, for example, choosing the smallest number or some
other property in which a single element can be distinguished

This axiom is perhaps the most perplexing and the most controversial of all the
axioms of ZFC.  It was added considerably later than the other axioms, which is
one reason why it is named specifically in the name of ZFC.  There are entire
books solely dedicated to AC and its conequences, and although we'll only look
at a very small portion of that material, it will become clear that with AC, we
end up with some truly bizarre and unintuitive results, but without it a huge
portion of mathematics becomes unprovable.

The Axiom of Choice (AC for short) has a long and sometimes bizarre history.
It was only added as an axiom after it was discovered that numerous axioms both
in set theory as well as other theories implicitly depended on its truth.
There were even some situations where mathematicians outright rejected AC, only
to find later that some of their own proofs relied on it.


