---
layout: math
title: ZFC - Overview
---

## A Quick overview

This first section will give a rough overview of what later sections will cover
in more detail.

### What is a Set?

We use the term "set" in everyday life to mean some kind of list,
collection, or group of items.  The U.S. presidents are set of people, the
noble gases are a set of natural elements, and the positive integers are a
set of numbers.  We can say things like "Abraham Lincoln is a U.S. President"
or "Oxygen is not a noble gas", or "there is no overlap between the set of even
integers and set of odd integers."

Set theory builds off of this concept but provides a more precise definition.
It captures the idea of a set in an abstract way so that we're able to make
well-reasoned assertions and statements about the nature of sets.  While this
degree of rigor might not seem very useful with the simple, finite sets
mentioned so far, it becomes absolutely essential when studying various types
of infinite sets, which is one of the primary goals of ZFC.  The world of
infinite sets is vast and often unintuitive, and a theory rooted in
formal logic like ZFC is required to make any sense of it all.

So let's now go through a more precise definition that is closer to the kinds
of sets that ZFC works with.  A **Set** is an unordered collection of distinct
items.  The items inside the set are called **members** or **elements**.  There
is no requirement for elements to be of a similar type, sets can contain other
sets, and there is a unique empty set (denoted by the symbol $\emptyset$).

A few quick examples of sets:

* $\\{1, 2, 3\\}$ is a set containing the numbers 1 through 3
* $\\{\text{apple}, \text{orange}, \text{banana}\\}$ is a set containing 3 words describing fruit
* $\\{\text{Abraham Lincoln}, \pi, ? \\}$ is a set of various unrelated things
* $\\{\\}$ is the empty set $\emptyset$, and $\\{\emptyset\\} = \\{\\{\\}\\}$
* $\nats$ is the infinite set of natural numbers $\\{0, 1, 2, 3, \dots\\}$



And some extra terminology and notation:

* Usually we use uppercase letters $A,B,\dots$ to denote sets and lowercase letters $a, b, \dots$ to denote elements of a set.  But be aware that since a set can be an element of another set, it is merely by context whether we refer to some set as $A$ or $a$.
* $a \in A$ mean that the element $a$ is an element of the set $A$
* $A \subset B$ means $A$ is a **subset** of $B$.  In other words, every element of $A$ is also an element of $B$ (but $B$ may contain elements that are not in $A$).
* Another way to describe a set is with set builder notation: $S = \\{x : x \in \nats ~\text{and} ~x > 10 \\}$ means $S$ is the set of natural numbers greater than 10.

There are a few primitive operations on sets:

Given two sets $A$ and $B$, the **union** of $A$ and $B$, denoted $A \cup B$ is the set containing all of the elements that are $A$ or $B$.  So for example

$$
\{1, 2, 3\} \cup \{3, 4, 5\} = \{1, 2, 3, 4, 5\}
$$

keeping in mind that despite $3$ being in both sets, it still only appears once
in the union since sets don't contain duplicates.  If $A \subset B$, then $A
\cup B = B$.

The **intersection** of two sets $A$ and $B$, denoted $A \cap B$, is the set of elements that are in both sets.  For example

$$
\{1, 2, 3\} \cap \{3, 4, 5\} = \{3\}
$$

If $A$ and $B$ have no elements in common, then the intersection is $\emptyset$.  If $A \subset B$, then $A \cap B = A$.

Given two sets $A$ and $B$ with $A \subset B$, the **compliement** of $A$
relative to $B$, denoted $A'$ is the set of elements that are in $B$, but not
$A$.  For example, if $A = \\{1, 2\\}$ and $B = \\{1, 2, 3\\}$, then $A' =
\\{3\\}$.  If $A = B$ then $A' = \emptyset$.  Also $A'' = A$.


### Pure Sets

Looking at the above examples, we have sets of a few different types of items:
a set of numbers, a set of words, etc.  When an element of a set is not a set
itself, it is called a _urelement_.

In ZFC however, the only sets we use are called "pure sets", which are sets
that only contain other sets (including $\emptyset$).  For example, a few pure
sets are :

* $\\{\\{\\}, \\{\\{\\}\\}\\}$
* $\\{\\{\\{\\{\\}\\}\\}\\}$
* $\\{\\{\\}, \\{\\{\\}\\}, \\{\\{\\}, \\{\\{\\}\\}\\}\\}$

Pure sets might seem a bit unintuitive at first, but we'll see that many
familiar mathematical objects like numbers and functions can be built out of
pure sets, so it is rarely the case that we ever actually think about pure sets
in the form above.  For example, the third example is actually the number $3$
in ZFC.  We will show later exactly how we arrive at this fact, but aside from
that demonstration, we then simply go under the pretense that $3$ and all of
the other natural numbers are part of ZFC.  

Nevertheless, the fact that everything in ZFC boils down to pure sets makes the
theory a lot cleaner and easier to work with.  This becomes especially
important when we start thinking about sets in a generalized notion, or are
making an assertion about a wide array of sets.  If at every point we had to
consider the possibility of something being either a set or a urelement, we
only burden ourselves with extra considerations and complexity.  It is also
worth pointing out that there has been research into comparing ZFC with
alternative formulations of set theory which do include urelements, and for the
most part it has been proven that, especially for what we'll be covering here,
urelements do not make the theory any stronger.  In other words, anything we
can prove about pure sets can also be applied to sets with urlements, and vice
versa.

### Why Sets?

Numbers, particularly the natural numbers $0, 1, 2, \dots$, are often thought of as the
foundation and cornerstone of all mathematics.  Most of the math used in
everyday life is squarely focused on numbers and the patterns that emerge within
them.  And indeed it is the case that numbers have been the dominant subject of
mathematics since its inception, with geometry being its only rival.

However as mathematics has become more and more complex, with new abstractions and
generalization being discovered about numbers, mathematicians found themselves
studying these abstractions on their own.  Instead of looking for patterns in
numbers, they looked for patterns in sets of functions of numbers.  Areas of study like
topology and abstract algebra began to take shape, where the numbers
become just one example of much more abstract concepts like groups, rings, and
vector spaces.

Set theory itself arose initially from the study of certain types of functions.
Georg Cantor, in his famous diagonal proof, showed that the set of real numbers
was in a very tangible sense larger than the infinite set of natural numbers.
Not long after, people began to realize that all of the abstractions and
generalizations known to date, including the real and natural numbers
themselves, could be translated into the language of sets.



Typically, ZFC is noted as serving two main purposes:

1.  The study of infinity.  By studying the properties of infinitely large
    sets, we can bring structure to a topic whose nature has been largely
regarded with ambiguity and uncertainty.  Beginning with Cantors proof, Set
Theory has opened the doors to a complex and rich heirarchy of levels of
infinity that is still a very active area of research.

2.  The foundation of modern mathematics.  It might seem odd that facts about
    everything from integers to fractals to differential equations can somehow
boil down to facts about sets, but not only is this true, it has been proven
that virtually all of what we consider modern mathematics can be described in a
rather small portion ZFC.  At the same time, we have an increasingly strong
understanding of what ZFC _cannot_ prove, and another active area of research
revolves around how "strong" of a theory ZFC is compared to other similar
foundational theories.



### What are Axioms?

At the root of ZFC are its axioms.  These statements form the basis of the
entire theory and everything else can be derived from them.

Every mathematical proof follows a somewhat similar formula.  An assertion is
made about some mathematical object, and an argument based on known facts
proceeds to logically demonstrate justification for the assertion's truth.
Sometimes a proof will somehow directly show that two things are equal or that
an object has some kind of property.  Other times the proofs are less direct,
sometimes showing that the antithesis of the assertion leads to a logical
contradiction.  In any case, every proof ever written bases its arguments on
previously established truths.  

But if every proof is built off of previously known facts, we need a starting
place.  The rules of logic do not allow for an infinite regression of
implications, so there must be some set of facts that have no proof and are
taken as assumptions.  These are the axioms.  An **axiom** is simply an
assertion that is assumed to be true.  We can then take a single axiom or a set
of axioms and use the rules of logic to prove new assertions known as
**theorems**.  Together, a set of axioms and all of the theorems that follow
from them are known as a mathematical **theory**.  Thus every known truth in
ZFC, and hence most of mathematics as a whole, can be traced all the way back
to its axioms.

There is no definite method to determine which assumptions we should choose to
take as the axioms of a theory.  Assume too little and a theory is too weak to
prove any meaningful theorems, but assuming too much can lead to a theory
breaking too far from intuition.  However one criterion that rules above all
others in its importance is consistency.  A theory is **consistent** if it does
not lead to any contradictions.  More specifically, a theory is inconsistent if
some statement can be proven both true in false.

So of course ZFC itself must be consistent, right?  After all it would be
ridiculous to base all of mathematics on an inconsistent theory.  Well...as it
turns out a man named Kurt Godel discovered that's not so easy.  ....

ZFC itself went through several iterations of adding and
rewriting various axioms before settling on the version that is used today.
Since then, each axiom has been studied in depth and we have a very solid
understanding of what exactly each axiom brings to the table in terms of
proving power.  

Axioms bridge our intuitive notion of some mathematical object with a more
precise definition.  In the case of ZFC, we are taking our intuitive concept of
a set and encoding it as best we can into a set of formal axioms.  

While axioms can be stated in ordinary English, nowawdays
and especially in the case of ZFC, they are written in the language of
first-order logic.  We will cover FOL in depth later, but in a nutshell it is a
purely symbolic language used to frame and prove logical arguments.  Almost
like a programming languange, sentences written in FOL have an absolutely
precise grammar with no room for ambiguity or multiple interpretations.

### The Simplified Axioms of ZFC

In plain English (and somewhat simplified), the axioms of ZFC are:

* **Axiom of Extensionality** : Two sets are equal if they contain the same elements
* **Axiom of Pairing** : if $A$ and $B$ are sets, then $\\{A, B\\}$ is also a set
* **Axiom of Union** : if $A$ and $B$ are sets, then $A \cup B = \\{x : x \in A ~\text{or} ~ x \in B\\}$ is a set.
* **Axiom of Zero** : $\emptyset$ exists
* **Axiom of Infinity** : $\nats$ exists
* **Axiom of Seperation** : if $A$ is a set and $\phi(x)$ is some predicate-logic formula (for example "$x$ is a number between 2 and 3"), then we can create a new set $B = \\{x: x \in A ~\text{and} ~ \phi(x) \text{is true}\\}$
* **Axiom of Replacement** : if $A$ is a set and $f(x)$ is a function that maps every element of $A$ to some other item, we can create a set $B$ which is the range of $f$.
* **Axiom of Power Set** : if $A$ is a set, we can create a set $\powerset{A}$ which is a set containing every possible subset of $A$
* **Axiom of Foundation** : a set cannot contain itself, nor can there be an infinitely descending sequence of sets.
* **Axiom of Choice** : if $A$ is a set of sets, then we can create a set $C$, which contains exactly one element of each element of $A$.

These are roughly in order of complexity, so don't worry if the axioms seem
less and less intuitive as the list progresses.  We will go into much more
detail as we examine each axiom individually
