---
layout: math
---

# Functions and Relations

Functions and relations are a critical component of ZFC and mathematics as a
whole.  The general idea of a function should be familiar to most people.
Whether we're talking about an algebraic function like $f(x) = 3x + 2$ or a
programming function like `printf`, the idea of an abstract "machine" that
receives input and produces output is a ubiquitous across logic, mathematics,
and computer science.  Relations are not quite as well-known, but will in fact
start with an overview of relations, including how we encode and use them in
ZFC.

## Relations

A **Relation** is a logical property that will either be true or false for some
object or a finite set of objects.  For example, we can define a relation
$E(x)$ on the natural numbers that is true if $x$ is even and false otherwise.
So $E(2)$ is true while $E(3)$ is false.

Every relation requires one or more parameters.  $E(x)$ requires just one, but
a relation like $M(x,y)$, which we'll define as true if $y$ is a multiple of
$x$, requires two parameters.  The number of parameters that a relation
requires is called its **arity**, where we use the terms "unary" for 1-arity,
"binary" for 2-arity, and so-on.

Relations in mathematics are actually quite common and we'll list a few example that should be familiar:

* Perhaps the most fundamental relation is equality.  $a = b$ can be written as $=(a,b)$, showing that it is a binary relation.  Of course the exact definition of $=$ depends on the context.  We've already seen that in ZFC the axiom of extensionality defines $=(a,b)$ as $\forall x (x \in a \iff x \in b)$.
* $< $ and $\leq$ are binary relations.

Representing a relation in ZFC is easy.  A relation $\mathcal{R}(x_1, x_2, \dots, x_n)$ of n-arity can be
represented as a set $R$ of n-tuples, such the members of the set represent those
parameters in which the relation is true.  In other words:

$$
\mathcal{R}(x_1, \dots, x_n) \iff (x_1, \dots, x_n) \in R
$$

For example, consider a relation $\relation(x,y)$ represented by the set :

$$
R = \{(1, 2) , (3, 4) , (5, 7)\}
$$

To answer the question if $\mathcal{R}(3, 4)$ is true, we simply check if $(3,4) \in R$.

### Important Properties of Relation

* **Reflexive** : $\forall a \mathcal{R}(a,a)$
* **Transitive** : if $\mathcal{R}(a,b)$, and $\mathcal{R}(b,c)$, then $\mathcal{R}(a,c)$
* **Symmetric** : if $\mathcal{R}(a,b)$ then $\mathcal{R}(b,a)$,
* **Antisymmetric** : if $\mathcal{R}(a,b)$ and $\mathcal{R}(b,a)$, then $a = b$

### Equivalence Relations

An import type of relation is the **equivalent relation**, which can be
considered a more generalized form of equality.  Generally equivalence
relations are meant to capture the ideas of different objects being effectively
the same in some context.  An equivalence relation is _reflexive_, _symmetric_,
and _transitive_.  Generally we use the symbols $~$ or $\equiv$ for equivalence
relations that are not equality, i.e. $a \equiv b$.

It should be clear that the axiom of extensionality satisfies all these
requirements.  If we have $a = b$, then clearly $b = a$.  Obviously $a = a$ and
if we have $a = b$ and $b = c$, then it should be clear that $a = c$.

Another example using $\nats$ is the concept of modular arithmetic.  For
example, an analog clock is an example.  If we imagine winding all the natural
numbers around a clock we form an equivalence relation such that $12 \equiv 0$,
$13 \equiv 1$, $14 \equiv 2$ and so on.  Relations like this are studied heavily in number theory.

A third example involves rotations of some object in space.  Rotating an object
90 degrees is equivalent to rotating it 450 degrees since it will be facing the
same direction after both rotations.  

An equivalence relation is often used to partition elements of set into
**equivalence classes**.  For example, in our clock equivalence relation on
$\nats$, since $1 \equiv 13 \equiv 25 \equiv 37$, they all belong to the same
equivalence class.  We can represent this class as $[1]$.  It should be clear
that there are similar classes for $[2], [3] \dots [12]$, but $[13] = [1]$.

Lastly, given a set $A$ and an equivalence relation $\equiv$, we can form the
concept of a **quotient** of $A$, denoted $A / \equiv$, simply by creating a
set of all equivalence classes.

### Orderings

A special type of relation is one that is used to describe an ordering of
elements of some set.  Remember that sets are by nature un-ordered, so for
example $\\{1, 2, 3\\}$ and $\\{2, 1, 3\\}$ are exactly the same set.  Even
with a set like $\nats = \\{0, 1, 2, \dots\\}$, on its own we do not "know"
that $2 > 1$.  We must encode that knowledge as a relation, and that relation
is an ordering.


There are several different common types of orderings, which we'll examine more
closely, but in general an **Ordering** (or just an "order") is a binary
relation $R$ associated with some set $S$.  Thus a set paired with an ordering
is known as an **ordered set**.  We will use the notation $\langle S, R
\rangle$ to represent an ordered set consisting of the set $S$ with ordering
$R$.  For example $\langle \nats , \leq \rangle$ would be the set of natural
numbers with the usual ordering (although be aware that $\leq$ is just a symbol
and does not necessarily represent the usual order, though we will be explicit
when that is so).

#### Pre-order

The simplest type of ordering is the preorder.  Given a set $S$, a **pre-order** on $S$ is an ordering that is _reflexive_ and _transitive_.


#### Partial Order

A **partial order** is a preorder that is also antisymmetric.  Partial orders are extremely important in set theory.

#### Linear Order

#### Well Order

Possibly the most important ordering in ZFC is the well-order.  A well-order (WO for short) has all the properties of a partial order with one additional requirement:

* **Well-ordering Axiom** : Every non-empty subset of $S$ contains an $\relation$-minimal element.

An ordered set $\langle S, \relation \rangle$ in which $\relation$ is a well-order is called a **well-ordered set**.



