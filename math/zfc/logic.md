---
layout: math
---

# A Brief Introduction to Formal Logic

Most people should be familiar with the general idea of a logical argument in a casual sense.  

Formal logic takes these ideas and turns it into a well-defined and very
precise language involving only a few symbols but with the power to prove a
great deal of logical and mathematical arguments.

Pure formal logic is generally not immensely useful on its own, but rather its
use comes from when we extend the basic set of rules to talk about mathematics.

### Boolean Algebra

We'll start with an even simpler form of logic, Boolean Algebra.  This is a
"calculus" (system involving a few symbols and rules about how to use them)
focused on statements that are either true or false.

With boolean algebra, we can form expressions of boolean symbols that result in
a value of either true or false.

The symbols of Boolean Algebra

* variables $p, q, r \dots$
* Logical And $\land$
* Logical Or $\lor$
* Logical Not $\neg$
* Parentheses
* The constants $T$ and $F$

And here are the rules for constructing Boolean expressions

* $T$ and $F$ are expressions (called constants)
* Any variable by itself is an expression
* if $A, B$ are expressions, then so is $A \land B$
* if $A, B$ are expressions, then so is $A \lor B$
* if $A$ is an expression, then so is $\neg A$
* if $A$ is an expression, then so is $(A)$

So a few quick examples of Boolean expressions are 

* $p \land \neg q$
* $\neg(p \lor q \lor r) \land F$
* $T \lor \neg T$

Some expressions don't have any variables and are known as **sentences**.
These are expressions like $T \lor \neg T$ or $F \land (F \lor \neg T)$.  In
every case, a sentence can always be reduced to either just $T$ or $F$ based on
the following rules:

* $T \land T = T$
* $F \land x = F$ where $x$ is either $T$ or $F$
* $F \lor F = F$
* $T \lor x = T$
* $\neg  T = F, \neg F = T$

More complex sentences can be broken down into these simpler components.  For example:

1.  $(T \lor F) \land \neg \neg F$
2.  $T \land \neg \neg F$
3.  $T \land \neg T$
4.  $T \land F$
5.  $F$

Sentences on their own are not particularly useful, since they're always just
more long-winded ways of writing $T$ or $F$.  What is much more useful is when
when an expression has at least one variable, which we call a formula.  Unlike
a sentence, a formula does not have an immediate reduction to a constant, but
depends on an _assignment_ of its variables, where we replace each variable
with a constant.  For example, consider the formula $p \land T$.  we can assign
$p$ to $F$ in which case the expression reduces to $F$, or we can assign $p$ to
$T$ in which case the expression reduces to $T$.

We often use the notation $\phi(p,q,r)$ to talk about a formula with variables
$p, q, r$ and we can refer to an assignment using the notation $\phi[T,F,T]$.

We can take note that any expression involving a constant can be easily reduced
to an expression without it.  For example, we can see that $\phi(p): p \land T$ is
basically the same as just $\phi(p) : p$.  When two different formulas always reduce to
the same constant with the same assignment of variables, they are
**equivalent**, and we use the notation $\phi(p) \equiv \psi(p)$ to say that the
formulas $\phi$ and $\psi$ are equivalent.

Formulas that have more than one variable can start to get quite complex.  In
particular, a formula with $n$ variables will have $2^n$ different possible
assignments.  In these cases, it can often be helpful to write out a _truth
table_, which simply enumerates the possible assignments of a formula.

### Propositional Logic

Boolean algebra gives us a grammar for constructing expressions that can be
true or false, but we do not yet have the ability to frame and prove logical
arguments.  For that we need to extend boolean algebra to **propositional
logic** which adds several _logical axioms_ that allow us to form new formulas
from given ones.   

This is the heart of any logical argument.  We take some given set of
_propositions_ and use them to show that some other statement is true or false.

Propositional logic builds on top of Boolean Algebra, but adds two important concepts.

1.  The concept of logical implication, along with the symbol $\to$.  If $A$
    and $B$ are boolean expressions, then so is $A \to B$.

2.  The notion of a proposition, which is a 

The most important concept in propositional logic is _Modus Ponens_, which is
the statement: given $p$ and $p \to q$, infer $q$.  In other words, if we know
some proposition $p$ is true, and we also know that $p \to q$ is true, then we
can deduce that $q$ is also true.  

### Predicate Calculus

_(This section needs more work)_

Predicate Calculus builds on propositional logic in a powerful and fundamental way by adding two new symbols called **quantifiers**.  These are

* $\exists$ - The existential quantifier
* $\forall$ - The universal quantifier

$\exists$ should be read as "There exists ..." and $\forall$ should be read as
"for all ..." (infact the LaTeX codes for these symbols are `\exists` and
`\forall`).  These symbols are used to express statements like "Every student
passed the test" or "There is at least one car that is not red".

Their usage is as such:

* if $\phi$ is a formula, then so is $\exists x \phi$, where $x$ may or may not be a variable in $\phi$.  The same applies to $\forall$.

For example, if $\phi(x)$ is the formula "$x$ is a human", then $\exists x
\phi(x)$ means "there exists at least one human" or more literally "There
exists some $x$ such that $x$ is a human".  Likewise $\forall x \phi(x)$ means
"everything is a human" or "for all $x$, $x$ is a human".  We can see that
adding a quantifier to a formula can drastically change its meaning.
