---
layout: math
---

# A Casual Introduction to Set Theory

ZFC, which stands for  _Zermelo Fraenkel Set Theory with the Axiom of
Choice_,is the most commonly accepted foundation of modern mathematics.  By
foundation, we mean that nearly all of mathematics from calculus to geometry to
algebraic topology can be translated into the language of ZFC.  Whereas a
mathematical field like number theory may treat the integers as the most
fundamental object, in ZFC everything including numbers are thought of as sets.

This article is intended to be a high-level introduction to ZFC.  We will cover
all the prerequisites and no math knowledge beyond basic algebra is needed to
read and understand this article.  That said, we'll be introducing new concepts
at a very fast pace, so don't worry if you find yourself needing to go over a
section several times to understand it.

### Who is this for?

For one thing, this is for myself.  There's a saying that "You do not truly
understand something unless you can explain it to a child."  After all, what
better way to test one's claim of understanding a subject than to effectively
teach it to someone else.  Making an attempt to write a comprehensive guide to
set theory in a way helps solidify my own understanding. 

That said, this text is squarely aimed at those who have little education in
logic and mathematics, but have a curiosity in the subject and are looking for
a high-level introduction with few prerequisites.  In particular, parts are
aimed at those who may not be sure what this is all about to begin with and are
looking for some motivation into why it's interesting.

I have purposely avoided attempting to write this as a textbook.  I have
personally found math textbooks, even those that I highly regard, to be overly
focused on the technical aspects and not enough on the background, motivations,
and implications.  So instead of droning on with definitions and proofs, I
attempt to provide more context about how each topic fit in with the larger
ideas, and about how various developments came to be.  We are lucky that Set
Theory and ZFC in particular has a well-documented history of its development
(which I would argue is still in progress) so there is every reason to
include some of the background to illustrate how we got to where we are today.

## Sections

[Overview](overview.html)

[Introduction to Logic](logic.html)

[The Axioms](axioms.html)

[Functions and Relations](functions.html)

[Orderings and Ordinals](ordinals.html)

[Cardinals](cardinals.html)


### Works Cited

While I am not going to cite every definition or fact, below is a list of books from which most of this text is based.

(coming soon lol)
