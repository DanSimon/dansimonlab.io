---
layout: math
title: Ordinals
---

## Ordinals

The ordinals are one of two number systems commonly used in set
theory, the other system being the cardinal numbers.  We'll show that the
ordinals include the natural numbers, but go much further and include numbers
representing infinite quantities.

In a nutshell, the ordinals are the counting numbers of ZFC and are useful in
all kinds of ways.  They provide a more generalized form of the concept of
mathematical induction (covered below), and in many ways can be thought of as
the backbone of the universe of ZFC.

#### Ordinals vs Cardinals

Cardinal numbers will be explained much more in their own section, but to quell
any immediate curiosity of why ZFC has two number systems, ordinals and
cardinals are the same when it comes to the finite, so every natural number is
both an ordinal and a cardinal, but work very differently with the infinite.
Whereas the ordinals represent a concept known as order-type and represent a
generalization of the idea of counting and iteration, cardinals represent a
different concept known as cardinality, which can be thought of as a
generalization of the concept of size.  

For example, if we have a finite set like $\\{a, b, c\\}$, we can ask ourselves
two questions.  First, how large is this set?  And second, how can we uniquely
label each element of the set.  Of course this set has $3$ elements, so we can
say its size, or cardinality, is $3$.  Likewise, we can use the numbers $0, 1,
2$ to label each element of the set, which are all of the natural numbers less
than $3$, so we can say that the order-type of this set is also $3$.

But when it comes to infinite sets, we can no longer use the same "thing" to
talk about both the cardinality and order-type of a set.  This is why we need
both the ordinals and the cardinals.  The ordinals give us a system to do this
unique labelling, and the cardinals give us a system to talk about size.

### Encoding the Natural Numbers as Sets

We saw that the axiom of infinity describes a set $x$ using two rules:

1.  $\emptyset \in x$
2.  If $y$ is an element of $x$, then $y \cup \\{y\\}$ is also an element of $x$

This is what we call a recursive definition of a set.  Rule 1 is the base case,
since it stating that a specific set belongs to $x$.  Rule 2 is the recusive
case and is applied to both the base case and itself, ad infinitum.

The best way to make sense of what $x$ looks like is to start building it.
We'll build a series of sets that each resemble finite portions of $x$,
starting with just $\emptyset$ and adding the required $y \cup \\{y\\}$ in each
stage.  

We'll say that the first stage, $x_1$ is just $\\{\emptyset\\}$.  This is what
$x$ would look like with just the base case applied.  But now the recursive
case states that if $\emptyset \in x$, then $\emptyset \cup \\{\emptyset\\}$,
or just $\\{\emptyset\\}$, is also a member of $x$, so we'll add that to $x_1$
to get $x_2 = \\{\emptyset, \\{\emptyset\\}\\}$.  But it doesn't stop there,
since now we have another member of $x$ to which we apply rule 2, so we know
that $x_3$ must contain $\\{\emptyset\\} \cup \\{\\{\emptyset\\}\\}$, and so
forth:

$$
\begin{array}{ccl}

x_1 & = & \{\emptyset\} \\
x_2 & = & \{\emptyset, \{\emptyset\}\} \\
x_3 & = & \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}\} \\
x_4 & = & \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}, \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}\}\} \\
x_5 & = & \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}, \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}\}, \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}, \{\emptyset, \{\emptyset\}, \{\emptyset, \{\emptyset\}\}\}\}\} \\
\dots \\

\end{array}
$$

It should be clear that we can keep doing this indefinitely, since every time
we add $y \cup \\{y\\}$, that set becomes the $y$ of the next stage.  

There is a much easier way to visualize this set though.  Instead of denoting
the empty set by the symbol "$\emptyset$", let's just use "$0$".  Then $x_1$
can be written as $\\{0\\}$, and instead of calling it $x_1$, let's just call
it "$1$".  Then $x_2$ can be seen as $\\{0, 1\\}$, and we can write it just as
"$2$".  So our above sequence becomes:

$$
\begin{array}{ccl}

0 & = & \emptyset \\
1 & = & \{0\} \\
2 & = & \{0, 1\} \\
3 & = & \{0, 1, 2\} \\
4 & = & \{0, 1, 2, 3\} \\
\dots \\
\end{array}
$$

Therefore, we can think of each of these finite approximations of $x$ as an
encoding of a natural number.  Every natural number $n$ is simply the set of
all smaller natural numbers.  Also, the operation $y \cup \\{y\\}$ can be
interpreted as $y + 1$, since for example 

$$
2 + 1 = 2 \cup \{2\} = \{0, 1\} \cup \{2\} = \{0, 1, 2\} = 3
$$

These sets that represent the natural numbers are the finite ordinals.
More specifically, these are the Von Neumann ordinals named after their creator
John Von Neumann.  They are not the only way of encoding the natural numbers
into ZFC, but for several reasons they are by far the most used method.  

So using our defintion of ordinals, we can rewrite the axiom of infinity as:

$$
\exists x (0 \in x \land \forall y (y \in x \to y + 1 \in x))
$$

Now it should be apparant that the $x$ described in the axiom is a set that
contains _all_ of the finite ordinals.  Since there are an infinite number of
such ordinals, the axiom can be interpreted as saying that an infinite set
exists.  More specifically, since the set $x$ contains all of our set-encoded
natural numbers, we can interpret the axiom as saying that $\nats$ exists.  

Instead of calling this set $x$, we use the symbol $\omega$ (lowercase Greek
letter omega).  $\omega$ is an extremely important set in ZFC and without it,
ZFC goes from being able to describe most of mathematics to only a very small
portion of it.

### The Formal Definition of an Ordinal

So far we've stated that the natural numbers are ordinals, but before we can
continue with additional ordinals that are not natural numbers, we can now
state the proper definition of what an ordinal actually is.  An **ordinal** is
the well-ordered set of all $\in$-smaller ordinals.  This definition is a
little dense so let's look at it a bit more in depth.

First, an ordinal is a well-ordered set, where the well-ordering relation is set membership itself.  This means 

$$
\alpha < \beta \iff \alpha \in \beta
$$

(using greek letters $\alpha, \beta$ to represent arbitrary ordinals).  This
should be clear with the natural numbers.  For example, since $3 = \\{0, 1,
2\\}$, we can clearly see that all the numbers smaller than $3$ are also
elements of $3$.  Since we are using $\in$ itself as a relation, we generally
do not need to state the ordinal $3$ as $\langle 3, \in \rangle$, since the
relation can be unambiguously defined by the set itself.

Second, an ordinal is a transitive set, which means:

$$
\alpha \in \beta \to \alpha \subset \beta
$$

Again, we can see that this fact is true with the natural numbers.  For
example, $2 \in 3$ and since $2 = \\{0, 1\\}$ we can see that $\\{0, 1\\}
\subset \\{0, 1, 2\\}$ and thus $2 \subset 3$.  This is a very important
property since it guarantees that an ordinal is only built from other ordinals
and does not contain some arbitrary non-ordinal set.

_It's important to note that the converse is not true.  For example $\\{0, 2\\}
\subset 3$, but $\\{0, 2\\}$ is not an ordinal and thus not a member of $3$._

### Infinite Ordinals

So what is the first ordinal that is not a natural number?  It is, in fact,
$\omega$ itself.  Let's verify that it actually satisfies the definition of an
ordinal.

* It is well-ordered.  It should be clear that any subset of natural numbers,
  even an infinite subset, is going to have a smallest element.  For suppose
that's not the case, that there is some subset $B$ without a minimal element.
$B$ must be infinite, since otherwise just choose the smallest element without
issue.  But in that case it would mean some $n \in B$ has an infinite number of
predecessors.  But since $n$ by definition contains all its predecessors and is
also by definition finite, this is a contradiction.  Therefore no such $B$ can
exist.

* It is transitive.  Given any $n \in \omega$, it is clear that all of the
  members of $n$ are also members of $\omega$.  Thus every element of $\omega$
is also a subset of $\omega$

$\omega$ is the first example of a **limit ordinal**.  Unlike that natural
numbers, it is not produced by an application of the $y \cup \\{y\\}$ rule, but
instead it can be seen as the end result of an infinite number of applications,
or more specifically it can be seen as the union of all ordinals smaller than
itself.  Of course, there is no finite operation that can produce $\omega$, which is
precisely why its existence must be taken as an axiom.  

Now that we have established $\omega$ as an ordinal, there is nothing to stop
us from continuing the same progression that generated the natural numbers.  If
we consider $\omega \cup \\{\omega\\}$, we now have a new ordinal that contains
not only all the natural numbers, but $\omega$ as well.  We can describe this
new ordinal as $\omega + 1$.  Likewise, we can create $\omega + 2, \omega + 3$,
and $\omega + n$ for any natural number $n$.  Lastly, we can group all the $\omega + n$
together to create a new limit ordinal $\omega + \omega$.  So not only does
$\omega + \omega$ contain all of the natural numbers and $\omega$, it also
contains every $\omega + n$.  We can abbreviate $\omega + \omega$ as $\omega
2$ (not $2 \omega$ !).

Let's briefly take a closer look at $\omega 2$.  We can think of it as taking
two copies of the natural numbers and deciding that one copy is intrinsicly
larger than the other copy.  Thus $\omega + 3 > \omega + 2$, but it is also
greater than every natural number $n$.

Of course it doesn't stop there.  We can define $\omega 2 + 1$ etc to create
all the $\omega 2 + n$, and then create $\omega 3$.  We can similarly define
$\omega  n$ for any $n$, and then create $\omega \omega$ as the set containing
all the $\omega n$.  We can denote this ordinal as $\omega^2$.  Whereas $\omega
2$ could be thought of as 2 copies of the natural numbers stuck together,
$\omega^2$ can be thought of as an infinite number of copies of the natural
stuck together.  We can index each copy of $\omega$ with a natural number $n$,
so we could speak of the 23rd copy of $\omega$ or say that the 456th copy of
$3$ (which would actually be the ordinal $\omega 456 + 3$) is greater than the
156th copy of 98 (which is $\omega 156 + 98$).  

Indeed $\omega^2$ appears to be a staggeringly large number, since it is
greater than all of those other $\omega n + m$ ordinals.  But we haven't even
gotten started yet.  It should be clear we can define $\omega^2\omega$ as
$\omega^3$, which we could (perhaps with great difficulty) visualize as an
infinite number of copies of $\omega^2$, and define $\omega^4$ etc similarly, and thus
$\omega^n$ for all $n$, then group them all into a new ordinal $\omega^\omega$.

At this point there is probably no intuitive way to visualize $\omega^\omega$,
since even an infinite number of copies of any of the $\omega^n$ is not
accurate.  But nevertheless it still very much satisfies the required
properties of being an ordinal, so we can continue to define $\omega^\omega +
1$, $\omega^\omega + \omega 3$, etc.  Eventually we get to defining
$\omega^{\omega + 1}$.  And if we make
that leap then it should be no surprise that we can define
$\omega^{\omega^\omega}$, $\omega^{\omega^{\omega^\omega}}$, and so on.

Trying to make any intuitive sense out of an ordinal like
$\omega^{\omega^{\omega^\omega}}$ is probably impossible, and yet that pales in
comparison to the next level in this heirarchy.  Clearly we have an ever-increasing "power tower" of ordinal exponentiation

$$
\{\omega, \omega^\omega, \omega^{\omega^\omega}, \dots\}
$$

Now consider the entire set of all these power towers.  We have arrived at a
very important ordinal known as $\epsilon_0$.  It is a limit ordinal that is
larger than all of the power towers, and has the counter-intuitive property of
being a fixed point.  In other words, $\omega^{\epsilon_0} = \epsilon_0$.

Thus we can see that ordinals generally fall into two categories.  A successor
ordinal is any ordinal $\alpha$ that can be written as $\beta + 1$ where
$\beta$ is an ordinal.  All other ordinals are limit ordinals.

### Ordinal Arithmetic

We've already see how we can add 1 to an ordinal, but now we can generalize
this a bit so that we can add any two ordinal together, including limit
ordinals, and we can define other operations like multiplication and
exponentiation.

#### Addition


