---
layout: math
---

$$
\newcommand{\ep}[1]{\varepsilon_{#1}}
$$

# A Look at Large Countable Ordinals

Aka "Let's see what's inside $\aleph_1$.

Large countable ordinals are interesting since they are all the same
cardinality as $\omega$, which means any such ordinal can describe functions
$\omega \to \omega$ that map to very, very large numbers.

## Small countable Ordinals

Ordinal addition is simple, basically $\alpha + \beta$ is just the order type
of smashing them together.  So $2 + \omega$ is like  $0_2, 1_2, 0\s{\omega},\\
1\s{\omega}, \dots$  which has order type $\omega$ since we can just map
$0\s{\omega} \to 2$ and so forth.  On the other hand $\omega + 2 = 0\s{\omega},
1\s{\omega}, \dots, 0_2, 1_2$, which is its own order type.  This works even
for very large ordinals, $\omega + \omega_1 = \omega_1$ but $\omega_1 + \omega$
is it's own order type.

Ordinal multiplication is just repeated addition, but again it's not
commutative.  $\omega 2 = \omega + \omega$, but $2 \omega = 2 + 2 + 2 + \dots$ 
is just $\omega$.

Ordinal exponentiation is a whole different beast, because it differs
significantly when the exponent is a successor vs a limit ordinal.  For
$\alpha^{n+1}$ is just $\alpha^n \dot \alpha$, makes sense.  With a limit
ordinal though $\alpha^\beta$ is the supremum of all $\alpha^\gamma$ with
$\gamma < \beta$.

This means that $\omega^\omega$ is the limit of all the $\omega^n$, which also
means it's just the set of them.  What about $\omega^{\omega^\omega}$?  Well
that is just the set of all ordinals of the form $\omega^\gamma$ for $\gamma < \\
\omega^\omega$, so it's just the limit of the $\omega^{\omega^n}$.

These 3 operations are the basic arithmetical operations on ordinals, and any
ordinal that can be represented using only finite ordinals, $\omega$ and
arithmetical combinations of them we'll call _small countable ordinals_.  For
exmaple:

$$
\omega^{(\omega^{(\omega^2 + \omega 3 + 287)} + \omega^{54}6 + \omega)} + \omega^{\omega 8} + \omega^{15}9876 + 1
$$

is pretty big, but still a small countable ordinal.


## $\varepsilon_0$

$\varepsilon_0$ has three equivalent definitions:

1.  The first fixed point in the function $f(\alpha) = \omega^{\alpha}$
2.  The supremum of the sequence $\\{\omega, \omega^\omega, \omega^{\omega^\omega}, \dots\\}$
3.  The set of all small countable ordinals

So we can say that it is the first "large" countable ordinal, in that it cannot
be described by the arithmetic ordinal operations.

## Ordinal Hyper-operations

What about an equivalent of up-arrow notation for ordinals?  Well if we stick
with the definition, then $\alpha \uparrow\uparrow \beta$ should be a power
tower of height $\beta$.  So $\alpha \uparrow\uparrow \omega$ should be the
limit of all finite power towers of $\alpha$.  So that means $n \uparrow\uparrow \omega = \omega$,
and $\omega \uparrow\uparrow \omega = \varepsilon_0$. 

In fact:

**$\ep{\alpha + 1} = \ep{\alpha} \uparrow\uparrow \omega$**. This is because

$$
\ep{\alpha + 1} = sup \big\{\ep{\alpha}, \ep{\alpha}^{\ep{\alpha}}, \ep{\alpha}^{\ep{\alpha}^{\ep{\alpha}}} \big\}
$$

This means that $\omega \uparrow^3 \omega$ is going to be very large.



Let's be a little more rigorous with this:

**Definition** : For all $n > 0 \in \omega$, $\alpha \uparrow^{n+1} \beta = \\{\alpha \uparrow^n sup(\alpha \uparrow^{n + 1} \gamma) : \gamma \in \beta\\}$

So why do we need to define it this way?  Well if we consider $\omega
\uparrow\uparrow \omega$, we can't let this translate exactly as you would with
a finite operand, since you cannot technically write:

$$
\omega \uparrow \omega \uparrow \omega \dots
$$

since that would be an infinitely long expression.  But of course we can "fake"
it by taking the limit of all the $\omega \uparrow n$, which at least in this
case is $\varepsilon_0$.

So then $\omega \uparrow^3 \omega$ is $\omega \uparrow^2 sup(\omega \uparrow^3 n) = \omega \uparrow^2 \ep{0}$.  And _that_ is equal to 

$$
\omega \uparrow sup \{\omega \uparrow^2 \gamma : \gamma \in \ep{0} \}
$$

So we know this is gonna be a lot bigger than $\ep{0}$, since $\omega
\uparrow^2 \omega$ is one of elements of that set, and one of the smallest ones
at that.

Maybe it's worth looking at $\omega \uparrow^2 (\omega \uparrow^2 n)$, since
those are unbounded in $\ep{0}$, the limit of those is also the limit we want.  So what is $\omega \uparrow^2 \omega^{\omega}$?  Well that is $\omega \uparrow sup\\{\omega \uparrow^2 \omega^n\\}$.  

ugh, ok so breaking it down even further, $\omega \uparrow^2 \omega^2 = \omega \uparrow sup\\{\omega \uparrow^2 \omega n\\}$... and then we have $\omega \uparrow^2 \omega + \omega = \omega \uparrow sup\\{\omega \uparrow^2 \omega + n\\}$.

So I guess this whole thing boils down to, what is $\omega \uparrow^2 (\omega +
1)$?  Well _that_ we can answer.  Remember, the supremum of all $\gamma <\\
(\omega + 1)$ is just $\omega$....but that means all the $\omega \uparrow^2
(\omega + n)$ are just $\ep{0}$, so then everything just goes to $\ep{0}$...I
guess.

After some internet research, a comment on [this
article](https://johncarlosbaez.wordpress.com/2016/06/29/large-countable-ordinals-part-1/)
seems to corroborate.  Up-arrorw notation from $\omega$ indeed gets "stuck" at
$\ep{0}$.  Another linked article from that article also corroborates the above
statement.

But according to [this
comment](https://johncarlosbaez.wordpress.com/2016/06/29/large-countable-ordinals-part-1/#comment-81223),
the issue seems to stem from the direction we expand the arrow notation.  If
instead we define it as:

$$
\alpha \downarrow^{n+1} \beta = sup\{\alpha \downarrow^{n+1} \gamma: \gamma \in \beta\} \downarrow^n \alpha
$$

then it should be clear that $\omega \downarrow^3 \omega = sup\\{\omega
\downarrow^3 n\\} \downarrow^2 \omega$.  If we expand $\omega \downarrow^3 2$ we get 

$$
(\omega \downarrow^3 1) \downarrow^2 \omega = ((\omega \downarrow^3 0) \downarrow^2 \omega) \downarrow^2 \omega = \omega \downarrow^2 \omega = \ep{0}
$$

And $\omega \downarrow^3 3$ becomes:

$$
\omega \downarrow^2 \omega \downarrow^2 \omega = \ep{0} \downarrow^2 \omega = \ep{1}
$$

So that must mean that the supremum is $\ep{\omega}$, which then gives us
$\ep{\omega} \downarrow^2 \omega$.  And that equal $\ep{\omega + 1}$.  That
seems like a rather odd result.  So maybe this is wrong?

**Hypothesis : $\alpha \downarrow^3 \omega = \ep{\alpha + 1}$**

Ok, now let's looks at $\omega \downarrow^4 \omega$, which expands to
$sup\\{\omega \downarrow^4 n\\} \downarrow^3 \omega$.  This looks like it's
gonna be a lot bigger than we think.  What does $\omega \downarrow^4 2$ look
like?  Well that's just $\omega \downarrow^3 \omega = \ep{\omega + 1}$.  and $\omega \downarrow^4 3$ is $\ep{\omega + 1} \downarrow^3 \omega$.

So that expands to $sup\\{\ep{\omega + 1} \downarrow^3 n

### An Alternative Definition

So it seems like both versions defined above don't really work, but a new player enters: [this](http://kaharris.org/teaching/582/Lectures/lec20.pdf) defines up-arrow notation in a much simpler manner

* $\alpha \uparrow^{n + 1} (\beta + 1) = \alpha \uparrow^n (\alpha \uparrow^{n+1} \beta)$  NOPE it still needs to be left-associative
* $\alpha \uparrow^n \beta = sup\\{\alpha \uparrow^n \gamma : \gamma \in \beta\\}$ - for a limit ordinal $\beta$

Now _this_ seems to make much more sense.

Let's re-examine what we looked at before:

First, $\omega \uparrow^2 \omega = sup\\{\omega \uparrow^2 n\\} = \ep{0}$, so we're good there.

Next $\omega \uparrow^3 \omega = sup\\{\omega \uparrow^3 n\\}$.  So if we look
at $\omega \uparrow^3 2$ we get $\ep{0}$ again, and $\omega \uparrow^3 3 =
\omega \uparrow^2 \omega \uparrow^2 \omega$, which is $\ep{0} \uparrow^2
\omega\\ = sup\\{\ep{0} \uparrow^2 n\\} = \ep{1}$.  So that means $\omega
\uparrow^3 n = \ep{n-2}$ and therefore the supremum is $\ep{\omega}$.

So then $\omega \uparrow^4 \omega$ is the supremum of $\omega \uparrow^3 n$  so
lets start by examining $\omega \uparrow^3 \omega \uparrow^3 \omega =\\
\ep{\omega} \uparrow^3 \omega$.  Ok, thus we much look at the $\ep{\omega}\\
\uparrow^3 n$ starting with 2, which is $\ep{\omega} \uparrow^2\\
\ep{\omega}$.  Uhhh.


## The Veblen Hierarchy

So we've seen that $\varepsilon_0$ is sort of the final destination for any amount
of ordinal arithmetic involving any ordinals smaller than it.  The next place to go then is with $\varepsilon_0 + 1$.  It should be clear that

$$
\omega^{\varepsilon_0 + 1} > \omega^{\varepsilon_0} = \varepsilon_0
$$

So if we again "push" $\varepsilon_0 + 1$ up with exponentiation, we get

$$
\varepsilon_1 = sup \big\{ \varepsilon_0 + 1, \omega^{\varepsilon_0 + 1}, \omega^{\omega^{\varepsilon_0 + 1}}, \dots \big\}
$$
