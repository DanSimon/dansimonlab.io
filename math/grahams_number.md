---
layout: math
---

# A Feeble Attempt to Visualize Graham's Number

There's already a decent amount of material about Graham's number on the
internet, this is just my attempt to reason through the definition and try to
paint a more intuitive picture of it.

Graham's number is astoundingly huge.  A "traditionally large" number like
$10^{10^{10}}$ (a number with 10,000,000,000 digits, aka a _googolplex_) doesn't
come anywhere close to it, like not even anything close to anything close.

Of course, the fact that there are an infinite number of integers means that
it's not surprising that Graham's number exists (and that there are much larger
numbers), but just that it is so much larger than any number you would find in
everyday life, even for mathematicians, and that it is relatively easy to
describe makes it interesting.

## Knuth's up-arrow notation

$$
\newcommand{\u}{\uparrow}
$$

This notation is the easiest way to explain Graham's number.  It has the form
$x \u^n y$, where there can be 1 or more arrows between the numbers, like $x
\u\u y$ or $x \u^{10} y$ to mean there's 10 arrows.

If we think about operations on natural numbers, they can all be describes as
more "powerful" versions of addition.  For example, $2 \times 3$ can be written
as $2 + 2 + 2$ and $2^3$ can be written as $2 \times 2 \times 2$.  Knuth's
notation basically generalizes this concept of _hyper-operation_.

The general form of up-arrow notation is

$$
\begin{array}{lcl}
x \u y & = & x^y \\
x \u^{n + 1} (y + 1) & = & x \u^n (a \u^{n+1} y)
\end{array}
$$

This is a recursive definition, meaning that for example $x \u\u y$ is defined
as repeated applications of $x \u x$.  For example, $2 \u\u 4$ is the same as $2 \u 2 \u 2 \u 2$ and 
$5 \u^4 3 = 5 \u^3 5 \u^3 5$.

## Building $G_1$

Let's start with $3 \u 3$.  This is just $3^3 = 27$, so that one's easy.

Next is $3 \u\u 3$, which is equal to $3 \u 3 \u 3$.  Well we already know $3
\u 3$ so we can write this just as $3 \u 27 = 3^{27} = 7,625,597,484,987$ (we'll just abbreviate that as 7.6T).
That's a pretty big jump up from 27, so we can see things are going to get
ridiculous pretty quickly.

Next is $3 \u\u\u 3$, which is the same as $3 \u\u 3 \u\u 3$.  Just as before, we can rewrite this as $3 \u\u 7.6T$.  But then that number is the same as

$$
3 \u 3 \u 3 \u 3 \u 3 \u 3 \u 3 \u 3 \u 3 \u 3 \u 3 \dots
$$

with $7.6T$ 3's appearing in the expression!  In other words $3 \u\u\u 3$ is a
"power tower" of $3^{3^{3^{3^{3}}}}$ but with 7.6 trillion 3's!!!  This is an
insanely huge number.  If you used all the matter in the known universe with
each digit the size of an atom, you would not even begin to be able to write it
out.  In fact you wouldn't even be able to write out how many digits that
number had.  You've actually gone way past that limit with only a few dozen
3's, so 7.6 trillion of them produces a number that dwarfs any possible
meaningful physical representation.

It should be obvious what's coming next, $3 \u\u\u\u 3$, which is finally the number we actually need to consider.  This is of course $3 \u\u\u 3 \u\u\u 3$.  But again, we just described $3 \u\u\u 3$ as this mindnumbingly large number, so that means this number is :

$$
3 \u\u 3 \u\u 3 \u\u 3 \u\u 3 \u\u 3 \dots
$$

with $3$ appearing that ginormously huge number of times.  So it's pretty clear
that _this_ number is going to be far beyond anything we can reasonably
comprehend.  But there is a slightly more sane way of visualising that.  We
already saw that $x \u\u y$ is like saying "a power tower of $x$'s of height
$y$." So that means we can think of this as creating a whole bunch of power towers,
where the _height_ of the next power tower is equal to _value_ of the previous
one.  

Start with Tower 1 as $3 \u\u 3 = 7.6T$, a power tower of height 27,
but producing a value of $7.6T$.  So then Tower 2 has a height of $7.6T$ and
producing a value equal to $3 \u\u\u 3$.  But then we keep going, Tower 3 has
height $3 \u\u\u 3$ and value SOME_HUGE_NUMBER.  Tower 4 has height
SOME_HUGE_NUMBER, and value EVER_HUGER_NUMBER.

We're only at Tower 4 and we've already completely blown past the value of
$3 \u\u\u 3$, but we keep creating more and more towers.  This keeps going for
a long, long, long time.  Finally we arrive at Tower $3 \u\u\u 3$.

$3 \u\u\u\u 3$ is known as $G_1$.  Is $G_1$ Graham's number? Hahahaha, no!  Not
even close.  Let me make that clear.  $G_1$ is some unfathomably high tower of
3's, but is nowhere even close to Graham's number.

So what's next then?  We use $G_1$ to create:

$$
G_2 = 3 \u^{G_1} 3
$$

Yes, that number has $G_1$ arrows between the 3's.  There is probably no way to
describe this number in any way that properly conveys how large it is.  But we
don't even stop there, we then create $G_3 = 3 \u^{G_2} 3$, as well as a $G_4$,
$G_5$, and so on, all the way up to $G_{64}$.  _This_ is Graham's number.


