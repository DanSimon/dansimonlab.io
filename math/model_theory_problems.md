---
layout: math
---

# Model Theory Problems

## Chapter 2

### 2.1.1

**Show that there are also countable nonstandard models of complete number theory**

We know from Lowenheim-Skolem that there are nonstandard models that are
uncountable.  We need to show that a reduct of one such model to countable
universe yields a model not isomorphic to the standard one.

Can we somehow append a new constant $c$ to the language of CNT, along with
statements that $c > n$ for all n.  Let $\mod{A}$ be a model of this language.
Then the reduct to CNT is a countable model of complete number theory not
isomorphic to the standard model.  Does this work?  

Maybe.  This exact approach is described
[here](http://math.stackexchange.com/a/1815554/11405), however this is for a
model of Number Theory, not _Complete_ Number Theory.  Wouldn't the statement
$\neg \exists x \forall y (x > y)$ hold in the standard model and thus not in
this language?  NO!!  When we add the constant $c$ to the language, as we said
we have to add a lot of other elements as well to satisfy the theory.  So of
course it means we add elements such that this sentence still holds.  In
particular we obviously need to add a successor chain of elements just to
satisfy PA itself.

A confirmed _yes_ is in Weiss's fundamentals of model theory pdf, where this
exact procedure is described precisely for the theory of the standard model.


So how do we know we can do this and not end up with some nonsense or inconsistent theory/model.  Well we can break this down into two steps.

First we know that CNT is maximal consistent since it is $Th(\nats)$.

Next, appending a new constant to CNT changes nothing about that fact.  None of
the statements in  $Th(\nats)$ mention $c$, so it is still consistent.

Now if we append $c > 0$, the theory is still consistent, since $c$ can be any
number $> 0$.  The same if we append $c > 1$, etc.  So if we append all the $c > n$, 
by compactness it is also consistent and therefore has a model.  By
Lowenheim-Skolem, there is thus a countable model and the reduct of that model
to $Th(\nats)$ cannot be isomorphic to the standard model, since although we
remove the constant symbol, all of those new elements are still there.

### 2.1.8

**Let $\mod{A} = \langle A, \leq, \dots \rangle$ be an infinite model such that
$\leq$ well orders $A$.  Show that there is a model $\mod{A}'$ equivalent to
$\mod{A}$ such that $\leq'$ is not a well-ordering.**

This appears to be similar to both the above proof and the proof that there
exist non-Archimedean ordered fields equivalent to $\reals$

We basically want to append a constant $c$ and some new sentences about it to
$Th(\mod{A})$ that "breaks" the well-ordering.  We can create a series of
languanges, where for each new language $\lan{L}\s{n+1}$ we add a constant
$c\s{n+1}$ and add the statement $c\s{n+1} \leq c\s{n} \land c\s{n+1} \neq
c\s{n}$ to $Th(\mod{A}).  It is clear that each subsequent theory is consistent
and has a model.

Take $\lan{L}$ to be the union of the $\lan{L}\s{n}$.  From compactness, the
combined theory has an infinite model and $\leq$ cannot be a well-order.


### 2.1.9

**Show that every infinite model $\mod{A}$ for a language $\lan{L}$ has an
equivalent model $\mod{B}$ of power $||\lan{L}||$ such that not every element
of $B$ is a constant of $\mod{B}$.**

First, by Lowenheim-Skolem, we can assume

$$
|A| = ||\lan{L}||
$$

Let's also assume that every element of $A$ is a constant of $\mod{A}$.

Looks like we can again do this with adding a new constant to $\lan{L}$, then
add statements to $Th(\mod{A})$ of the form $c \neq d$ for each constant symbol
$d$ of $\lan{L}$.  Again by compactness the resulting theory has a model
$\mod{B}$ which is also a model of $Th(\mod{A})$ and is thus equivalent to
$\mod{A}$, and again by Lowenheim-Skolem we can assume $|B| = ||\lan{L}||$.
Yet since $\lan{L}$ doesn't contain the symbol $c$, there must be at least one
element of $B$ that is not an interpretation of any of the $d$.

### 2.1.10

**Let $\lan{L}$ have no function or constant symbols.  Let $T$ be a theory in
$\lan{L}$ and $\mod{A}$ a model for $\lan{L}$.  Then $\mod{A}$ is
homomorphically embedded in some model of $T$ _iff_ every finite submodel of
$\mod{A}$ is homomorphically embedded in some model of $T$.**

Lets assume every finite submodel of $\mod{A}$ is embedded in some model $\mod{B}$ of $T$.  This means for each relational symbol $R$, we have:

$$
R\s{\mod{A}}(x_1 \dots x_n) \to R\s{\mod{B}}(f(x_1) \dots f(x_n))
$$

Let's take two approaches to this:

#### Following the proof of corollary 2.1.9

That is basically the same as this except for isomorphic embeddings.  

If we show that $\Sigma = T \cup \Delta\s{\mod{A}}$ is consistent, does that
prove the exercise?  No, but it might work for the _positive_ diagram.  Yes,
prop 2.1.12 implies that if $\mod{B}$ is a model of the positive diagram of
$\mod{A}$, then $\mod{A}$ is h-embedded in $\mod{B}$.

It appears that the rest of the proof follows exactly, so I guess we're done here.

### 2.1.11

**Let $\mod{A}$ be an arbitrary infinite model and let $\alpha \geq
||\lan{L}||$.  Then there is a model $\mod{B}$ equivalent to $\mod{A}$ such
that for every $\phi(x)$ with one free variable, if $\phi(x)$ is satisfied by
infinitely many different elements of $\mod{B}$, then $\phi(x)$ is satisfied by
$\alpha$ different elements of $\mod{B}$.**

Let's assume that for some $\phi(x)$, it is satisfied by $\beta > \alpha$ elements of $\mod{B}$.

Let's assume that every element of $\mod{A}$ is a constant.  We can a new
constant $d$ and statements $\phi(d)$ and $d \neq c$ for each $c$.  We should
be able to do this $\alpha$ times for each $\phi(x)$.  The resulting model is
the desired $\mod{B}$.

Let's make this a little more precise.  Let's enumerate the constants of
$\mod{A}$ as $c\s{\gamma}$ and all formulas with one free variable as
$\phi\s{\gamma}$.  For $\beta < \alpha$, we will construct new languages $\lan{L}\s{\beta}$.  

For $\beta + 1$, we add a new constant symbol $d\s{\beta + 1, \gamma}$ for each $\phi\s{\gamma}$.  We also add the sentence

$$
\phi\s{\gamma}(d\s{\beta + 1, \gamma})
$$

along with sentences of the form $d\s{\beta + 1, \gamma} \neq d\s{\delta,
\gamma}$ for all $\delta < \beta + 1$.  This could be further broken down by
create a series of languanges and theories.  So ultimately for a single $\phi$,
we must consider adding a single new constant $d$ and sentences $\phi(d)$ and
$d \neq c$ for all previously defined $c$.  Since we know infinitely many
elements already satisfy $\phi$, these additions are consistent.

For limit ordinal take the unions.

Then the resulting language $\lan{L}\s{\alpha}$ has $\alpha$ many constant
symbols for each $\phi$ along with a theory $T\s{\alpha}$ that ensures all the
constants for each $\phi$ are distinct.  Let $\mod{A}\s{\alpha}$ be a model of
$T\s{\alpha}$.  The reduct of this model to $\lan{L}$ thus has $\alpha$ many
elements satisying any formula $\phi(x)$ that is satisfied by infinitely many
elements.

Something about that doesn't seem quite right....

Let's try another approach.  If we can enforce the fact that every $\phi(x)$ is
satisfied by $|A|$ elements of $\mod{A}$, we are done since we can just use
upward Lowenheim-Skolem.

In other words, there must be some set of sentences that can enforce that
"enough" elements of $\mod{B}$ satisfy $\phi$, and then use Upward-LS to get
the model of the correct size.

What if for a particular $\phi(x)$, we consider an infinite model $\mod{B}$ of $\phi$, and by upward LS, we can assume 

### 2.1.14

**Let $T\s{0} \subset T\s{1} \subset \dots$ be a strictly increasing set of
closed theories in $\lan{L}$.  Show that the union $\bigcup\s{n <
\omega}T\s{n}$ is a consistent closed theory in $\lan{L}$ that is not finitely
axiomatizable.**

We'll assume all the $T_n$ are consistent, otherwise there's no way the union could be consistent.

Next, since each theory is closed, and each $T\s{n + 1}$ is strictly larger
than $T_n$, there must be some sentence of $T\s{n+1}$ not provable in $T_n$.
So even if every $T_n$ is finitely axiomitizable, the union cannot be.

To show the union is consistent follows from compactness.  Obviously each
finite subset of each $T_n$ has a model, and since each finite subset of the
union is a finite subset of at least one of the $T_n$, the union also has a
model and is consistent.

Lastly, it is closed because each $T_n$ is closed.  If some consequence of the
union was not included, it would also be a consequence not included in one of
the $T_n$, a contradiction.

