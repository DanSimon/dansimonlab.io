---
layout: math
---

$$
\newcommand{\u}{\uparrow}
\newcommand{\d}{\downarrow}
\newcommand{\ep}[1]{\varepsilon\s{#1}}
$$

# Hyperoperations on Countable Ordinals

Summary : It seems like Knuth's up arrow notation does not work very well with
transfinite countable ordinals, and almost every treatment of it that I've
found on the internet gets it wrong.

$$
\begin{array}{lcl}
x \u y & = & x^y \\
x \u^{n + 1} (y + 1) & = & x \u^n (a \u^{n+1} y)
\end{array}
$$

Thus something like $2 \u\u 3 = 2^{2^2}$.

So how do we extend this to handle transfinite ordinals, in particular limit ordinals and fixed points.  This is where things seem to fall apart.

### The Right-Associative Version

This is the most direct approach at extending up-arrow notation to work with
limits, and defines $\alpha \u^n \beta$ as:

$$
\alpha \u^n \beta = sup \big\{\alpha \u^{n'} sup \{\alpha \u^n \gamma : \gamma < \beta\} : n' < n \big\}
$$

So if we look at $2 \u^2 3$, since $sup \{x : x < n + 1\} = n$, we can see that
this will expand to $2 \uparrow (2 \uparrow 2)$, which lines up with the original definition.

$\omega \u\u \omega = \ep{0}$, this works fine.

But what happens if we try to evaluate $\omega \u\u \omega + 1$?  We get
$\omega \uparrow (\omega \u\u \omega) = \omega^{\ep{0}} = \ep{0}$.  The same is
for any $\omega + n$, which ultimately means that $\omega \u\u \beta, \beta\\
\geq \omega = \ep{0}$.

Conclusion : naively extending up-arrow notation to include limits does not work and gets "stuck" at $\ep{0}$.

### The Left-Associative Version

It seems like the reason the above version doesn't work is because of the associativity of the expansion.  We need $\omega \u^2 \omega + 1 > \omega \u^2 \omega$, and it seems like a reasonable choice could be $\ep{0}^\omega$.  That is the idea behind [this
comment](https://johncarlosbaez.wordpress.com/2016/06/29/large-countable-ordinals-part-1/#comment-81223), which describes a left-associative version.


$$
\alpha \downarrow^{n+1} \beta = sup\{\alpha \downarrow^{n+1} \gamma: \gamma \in \beta\} \downarrow^n \alpha
$$

This function grows much more slowly, even with just $\omega \d^2 \omega$.  For example, $\omega \d^2 3 = (\omega^\omega)^\omega = \omega^{\omega^2}$, which shows that $\omega \d^2 \omega = \omega^{\omega^\omega}$.

$\omega \d^2 \omega + 1$ is then $\big(\omega^{(\omega^\omega)}\big)^\omega = \omega^{\omega^{\omega + 1}}$.  So it should be clear that $\omega \d^2 \omega + \omega = \omega^{\omega^{\omega + \omega}}$, and similarly for $\omega \times n$.

Thus $\omega \d^2 \omega^2 = \omega^{\omega^{\omega^2}}$, and $\omega \d^2 \omega^\omega = \omega^{\omega^{\omega^\omega}}$.  We can generalize this as, for $a,n < \omega$ :

$$
\begin{array}{ccc}
(\omega \u^2 a) \d^2 2 & = & \omega^{(\omega \u^2 a-1 \times \omega \u^2 a)} = \omega \u^2 a + 1 \\
(\omega \u^2 a) \d^2 n + 1 & = & ((\omega \u^2 a) \d^2 n)^(\omega \u^2 a) = (\omega \u^2 a + 1) \u n\\
\end{array}
$$

Which then means:

$$
\omega \u^2 a) \d^2 \omega = \ep{0}
$$

and

$$
\omega \d^2 \ep{0} = \ep{0}
$$

Ok, so what about $\omega \d^3 \omega$.  That is $sup\\{\omega \d^3 n\\}$.  So let's start with $\omega \d^3 3 = (\omega \d^2 \omega) \d^2 \omega = \omega^{\omega^\omega} \d^2 \omega = sup \\{\omega^{\omega^\omega} \d^2 n\\}$.  For $n = 2$ we get:

$$
\Big(\omega^{(\omega^\omega)}\Big)^{({\omega^{(\omega^\omega)}})} = \omega^{\Big(   (\omega^\omega) \times  ({\omega^{(\omega^\omega)}}) \Big)}= \omega^{\omega^{\omega^\omega}}
$$.

And for $n = 3$ we get $\omega^{\omega^{\omega^{\omega^2}}}$, which will mean the limit is $\omega \u^2 5$.  So to recap this means:

$$
\begin{array}{ccc}
\omega \d^3 2 & = & \omega  \u^2 3 \\
\omega \d^3 3 & = & \omega \u^2 5 \\
\end{array}
$$



### A New Version
