---
layout: math
---

# A Casual Introduction to Cantor's Diagonal Proof

Cantor's diagonal argument is a mathematical proof that offers a profound
insight into the nature of infinity and played a key role in completely
transforming mathematics in the late 19th century.  One of the most influential
proofs in the history of mathematics, it gave birth to the field of Set Theory
which has played a pivotal role in the development of modern mathematics.
While Set Theory itself is an immensely vast and rather abstract area of study,
Cantor's diagonal argument is surprisingly straightforward and only requires an
elementary understanding of the theory's fundamentals.  This article will cover
those prerequisites as well as the proof itself and a few closely related
ideas. 

In one sentence, _Cantor's diagonal argument proves that infinity comes in
multiple sizes._ The proof itself is not very complicated, and most of the work
is actually just deciding on proper definitions of both "infinity" and "size".
As we'll see, Set Theory provides the framework to achieve both of these goals.



## A Few Thoughts About Infinity

The idea that there are multiple sizes of infinity
may seem absurd.  We often think about infinity
as something that is limitless or unbounded.  An infinite staircase can never
be climbed nor can an infinitely long book ever be finished.  So how can
one infinite staircase somehow be larger than some other infinite staircase?

Let's take a look at a classic example called _Hilbert's Hotel_ (named after
the late 19th century mathematician David Hilbert) that demonstrates how
infinity can result in bizarre thought experiments.  Imagine a hotel with an
infinite number of rooms, each labeled with a natural number $1, 2, 3, 4,
\dots$, and every single room is occupied.  An infinitely large hotel filled by
an infinite number of guests.  Now a new person shows up looking for a room.
At first it might seem like they're out of luck since we already said that
every room is occupied.  But the hotel manager has a trick.  He asks the
occupants of room 1 to relocate to room 2, and the occupants of room 2 to move
to room 3, and so on.  In effect, the occupant of room $n$ moves to room $n +
1$.  It should be clear that every guest in the hotel will be able to move to a
new room, but now room 1 is free for the new guest.

But next, an infinitely large bus rolls up with an infinite number of
passengers, all looking for their own rooms.  The manager's trick won't work
here.  After all, we can't tell the room 1 guest to move to room $\infty$,
since "$\infty$ is not a natural number.  But the manager has yet another
trick.  The guest of room 1 will still move to room 2, but then the guest in
room 2 moves to room 4, and the guest of room 3 moves to room 6.  So the guest
in room $n$ moves to the room $2n$.  This means that all of the odd numbered
rooms are free, and every passenger on the bus can get their own room.

So what can we see from Hilbert's Hotel?  Is this thought experiment proof that
$\infty = \infty + 1$, or that $\infty = \infty + \infty$?  It seems so, but
this is hardly rigorous enough to be a mathematical proof.  The problem is
"infinity" as we've described it so far is a loosely defined concept, full of
ambiguity.  We haven't really proven anything since we haven't properly defined
what we're even talking about!  For the moment then, let's stop talking about
infinity as this very vague "$\infty$" and talk about something more concrete,
natural numbers.  These are the whole numbers larger than or equal to 0.  We
use the symbol $\nats$ to refer to the entire set of natural numbers, and as
we'll see, $\nats$ is one of the most important sets because it is the simplest
(and smallest) infinite set.

## A Very Brief Introduction to Set Theory

Since Cantor's proof deals with sets, we need to briefly cover what exactly we
mean by a set.  Intuitively a set is just a collection of objects, something
like an imaginary paper bag.  In everyday life the words "set", "list",
"collection", and "group" are mostly synonymous, but in set theory we establish
a very logically precise notion of a set and some rules about what kinds of
sets you can and can't create.  

A **Set** is a collection of distinct items.  We refer to the items in a
set as "elements" or "members".  We can have sets of numbers, sets of
people, sets of ideas, and even sets of sets (think of taking an empty paper
bag and putting it inside another paper bag), and elements do not need to be
the same type or category.  We use curly braces to denote a set, so $\\{1, 2, 3,
4\\}$ is a set containing the numbers 1 through 4, $\\{\text{horse}, \text{dog},
\text{cat}\\}$ is a set of animals, and $\\{=, \Sigma, \to\\}$ is a set of
mathematical symbols.  The empty set $\\{\\}$ is especially important and is
usually represented by the symbol $\emptyset$.

A few quick points to get a better grasp of sets:

* Sets are unordered, so $\\{a, b, c\\} = \\{c, b, a\\}$.  To be more specific, two sets are equal if and only if they contain the same elements.
* Sets do not contain duplicates, so $\\{a, b, b, c\\} = \\{a, b, c\\}$
* Sets can contain other sets, so $\emptyset, \\{\emptyset\\}$ and $\\{\\{\emptyset\\}\\}$ are all different sets.
* Sets cannot contain themselves

For a much more in depth look at Set Theory, see the article on [ZFC](zfc).
But everything mentioned here is all you really need to know.

## Cardinality

How are we going to use sets to prove there are multiple sizes of infinity?
The basic idea is we are going to describe these sizes of infinity by looking
at the sizes of various infinite sets.  If we can take two infinite sets and
prove that one contains more elements than the other in a non-paradoxical way,
then we've effectively shown that the two sets, while both infinite, have
distinct sizes.

Our first step is to better explain what me mean by "size".  Consider the set

$$
A = \{a, b, c, d\}
$$

How big is $A$?  We can easily count that it has 4 elements, so we say that the
**cardinality** of $A$ is 4.  Another way to write this is:

$$
\card{A} = 4
$$

If we have another set like 

$$
B = \{\text{apple}, \text{orange}, \text{banana}, \text{tomato}\}
$$

we can also count that it has 4 elements, and we can say that $A$ and $B$ have
the same cardinality, or that they are "isomorphic".  This can be written as
$\card{A} = \card{B}$.  In every case, we can compare the cardinality of any
two sets and write equations like $\card{A} > \card{B}$ or $\card{A} \leq
\card{B}$ to indicate that the cardinality of one set is greater/less than
another.

Now this makes total sense for finite sets, but what about an infinite set like
$\nats$?  What is $\card{\nats}$?  Counting is not going to work here, since we'd
never finish.  There is no natural number that we can use to describe $\nats$'s
cardinality. This is often where the symbol $\infty$ might be used, but
remember we're not allowed to talk about intuitive infinity.  Then we are stuck
with a set whose cardinality cannot (yet) be properly defined.  Cantor, in this
same situation, decided to use the symbol $\aleph_0$ (called "aleph zero" or
"aleph null") to represent the cardinality of $\nats$.  Therefore, we can now
say:

$$
\card{\nats} = \aleph_0
$$

Basically we're just creating a new symbol here to uniquely identify
$\card{\nats}$.  We're not yet saying what or how large this cardinality
_really_ is, we're just indicating that it cannot be described by a natural
number.  Intuitively we could say that $\card{\nats| > |A}$ , or $\aleph_0 >
4$, but how can we demonstrate that idea in a logically rigorous way?  After
all, we literally just made up a new symbol, yet by writing $\aleph_0 > 4$ we
are trying to treat $\aleph_0$ like a natural number.  

Furthermore, while it's clear that the infinite set $\nats$ has more elements than any finite set, lets say we have the set
$\nats^{E} = \{2, 4, 6, 8, ...\}$ of all even natural numbers.  How can we determine if
$\card{\nats| > |\nats^{E}}$?  You might try to argue that $\nats$
contains every element of $\nats^{E}$, but not the other way around (in
other words $\nats^{E}$ is a "subset" of $\nats$, which we write as
$\nats^{E} \subset \nats$).  I'll cut to the chase and say you'd be
wrong, they actually have the same cardinality!  

What we need is a better method to compare the cardinality of sets that works just as well when one or both sets are infinite, and doesn't involve manually counting the elements of each set.  

The tool we will use is what's known as a **bijective mapping**, also called a
bijection.  If we have two sets $A$ and $B$, a bijective mapping is a pairing
of elements from each set, such that every element from $A$ is paired with
exactly one element from $B$, and vice versa.  We can then make the following
assertion:

_For any two sets $A$ and $B$, $\card{A} = \card{B}$ if and only if a bijective mapping exists between them._

Thus is we're able to uniquely pair up elements together from each set, then we
can be sure that they contain exactly the same number of elements.  If not, it
will mean that one set will have too many elements and cannot be paired.

So for our first two sets:

$$
\begin{array}{ccc}
A & = & \{a, b, c, d\} \\
B & = & \{\text{apple}, \text{orange}, \text{banana}, \text{tomato}\} \\
\end{array}
$$

We can create the following bijective mapping:

$$

\begin{array}{ccc}
A &\iff  & B \\
\hline
a &\iff &  \text{apple} \\
b &\iff & \text{banana} \\
c &\iff & \text{orange} \\
d &\iff & \text{tomato} \\
\end{array}
$$

demonstrating that the two sets have the same cardinality.  Of course this is
only one of many possible such mappings.  We can rearrange the elements on
either side however we want and the mapping will still be perfectly valid.  At
no point do we really care which element maps to which, only the fact that
every element from each set is uniquely paired.

Now, if we have two sets:
$$
A = \{apple, orange, banana, grape\}, B = \{carrot, potato, onion\}
$$
it's obvious that $\card{A| > |B}$, but we can actually write a real proof to mathematically prove this assertion.  In this case the simplest proof would be to just write every possible attempt at pairing the elements together, and showing that in every single possibility (there are 648 total), there is always an unpaired element of $A$.

### Countably Infinite Sets

Now that we have a better way of comparing infinite sets, let's re-examine Hilbert's hotel.  We can define the problem more concretely by comparing two sets, $\nats = \{0, 1, 2, 3, ...\}$, and $\nats^- = \{1, 2, 3, 4, ...\}$.  $\nats^-$ can be thought of as the hotel in it's original state, and $\nats$ can be the hotel with the newly arrived guest.  We can see there is exactly one element of $\nats$ that is not in $\nats^-$, but does that mean $\card{\nats} > \nats^-$?

To prove that these two sets are isomorphic, we need to create a bijective mapping between them.  Basic algebra can help us here, and we can define a simple function $f: \nats \rightarrow \nats^-$ as:

$$
f(n) = n + 1
$$


This gives us a mapping that looks like:

$$
\begin{array}{ccc}
\nats &\iff & \nats^- \\
\hline
0 &\iff & 1 \\
1 &\iff & 2 \\
2 &\iff & 3 \\
... &\iff & ...
\end{array}
$$

We could have just as easily defined the inverse function $f^{-1}(n) = n - 1$ from $\nats^- \to \nats$.  In both cases, every element from one set is uniquely paired with an element from the other.  No element is paired twice and no element from either set is left unpaired.  Therefore we can conclude $\card{\nats} = \card{\nats^-}$.

We can see that two ideas that are the same for finite sets now differ for
infinite sets.  It is definitely the case that $\nats$ contains every element
of $\nats^-$ as well as an additional element $0$.  If these were finite sets,
it _would_ mean that one set was smaller than the other, but with infinite sets
that it not the case.  It is possible for an infinite set to have the same
cardinality as one of its subsets (in fact this can be seen as a necessary
condition to consider a set infinite).

Now let's examine another example we mentioned earlier: comparing $\nats$ to
$\nats^E = \\{0, 2, 4, 6, 8, ...\\}$, the set of all even natural numbers.  Again
we have a case of comparing an infinite set to a subset of itself, but in this
case the subset is missing an infinite number of elements: all of the odd
integers.  Of course, we already said that these two sets have
the same cardinality, but now it should be more evident how this is true.  We
can create a bijective mapping from $\nats$ to $\nats^E$ simply by mapping
every natural number to its double:

$$
\begin{array}{ccc}
\nats &\iff & \nats^E \\
\hline
0 &\iff & 0 \\
1 &\iff & 2 \\
2 &\iff & 4 \\
3 &\iff &6 \\
\dots &\iff & \dots
\end{array}
$$

So we have shown that $\card{\nats^E} = \aleph_0$.  Of course, this doesn't
just work for the even numbers, it works for any set of multiples.  In fact,
any infinite subset of $\nats$ has cardinality $\aleph_0$, which in turn is
proof that $\aleph_0$ is the smallest infinite cardinality.

The definition of cardinality states that two sets are isomorphic if a
bijective mapping exists between them, but it doesn't say that _every_ mapping
between them must be bijective.  After all we could create a mapping from
$\nats^E$ to $\nats$ just by mapping each even number to itself, which is
clearly not bijective.  That fact will be very important with our next example,
$\mathbb{Q}$, the set of all rational numbers.  Recall that a rational number
is just a fraction of the form $\frac{p}{q}$, where $p,q \in \nats$.  So
$\frac{1}{2}, \frac{2}{3}, \frac{4}{1}$ are all elements of $\mathbb{Q}$, but
irrational numbers like $\sqrt{2}$ or $\frac{\pi}{2}$ are not elements (in the
second case even though we have a fraction, $\pi$ is not an element of $\nats$,
so the fraction is not a rational number).

At first, $\mathbb{Q}$ appears to be an order of magnitude larger than $\nats$.
After all, there are an infinite number of fractions in between every two
integers.  Actually, pick any two rational numbers and there are an infinite
number of rationals in between them.  No matter how "close" the two numbers you
pick may be, there are always an infinite number of rationals in between them.
Having this property means that the rational numbers are a "dense" set, whereas
the natural numbers are not dense.  How can we possibly create a bijective
mapping between them?

Here's where our definition of a set really helps us out.  Remember that sets
are unordered, so the fact that the rational numbers are dense is actually
meaningless when talking about cardinality.  Density relies on being able to
say that one number is larger than another.  While we intuitively know about
the ordering of rational numbers, that ordering is not information contained
within the set.  Thus density has no correlation with cardinality (except in
the sense that it does mean the set is infinite).

Therefore, if we can figure out a clever way to re-order the rationals, we may
yet be able to line them up with the natural numbers.  In fact, there are many
ways to do this.  The trick is to not enumerate over just one part of the
fraction.  For example, if we tried to pair $n$ with $1/n$, rationals like
$\frac{2}{3}$ would not get paired.

Here is one example of a bijective mapping; in fact this was the example that
Cantor used.  We can lay out all the rational numbers along a grid starting
with $\frac{1}{1}$ in the upper left corner.  Moving over a column increases
the denominator, while moving down a row increases the numerator:

$$
\begin{array}{ c c c c c }
\frac{1}{1} & \frac{1}{2} & \frac{1}{3} & \frac{1}{4} & ...\\
\frac{2}{1} & \frac{2}{2} & \frac{2}{3} & \frac{2}{4} & ...\\
\frac{3}{1} & \frac{3}{2} & \frac{3}{3} & \frac{3}{4} & ...\\
\frac{4}{1} & \frac{4}{2} & \frac{4}{3} & \frac{4}{4} & ...\\
... \\
\end{array}
$$

Now we start in the corner, and start to zig-zag down the grid

$$
\begin{array}{ccc}
\nats & \iff  & \mathbb{Q} \\
\hline
0 &\iff &\frac{1}{1} \\
1 &\iff &\frac{1}{2} \\
2 &\iff &\frac{2}{1} \\
3 &\iff &\frac{3}{1} \\
4 &\iff &\frac{2}{2} \\
5 &\iff &\frac{1}{3} \\
\dots &\iff& \dots
\end{array}
$$

With this mapping, we're not just zooming off in one direction forever.  The
zig-zagging ensures that every rational will get mapped at some point.  This is
obviously not as simple as the algebraic mappings we've done previously, but
this mapping is bijective and proves that $\card{\nats} = \card{\mathbb{Q}}$.

There is another way to create a bijective mapping between $\nats$ and $\rats$
that is a bit more clever and uses prime numbers.  Recall that a natural number
is prime if it is only divisible by itself and 1.  So 5, 13, and 29 are all
prime numbers, but $4 = 2 \times 2$, $27 = 9 \times 3$, and $8192 = 512 * 16$
are not prime, which we call composite numbers.  Second, every composite number
has a unique prime factorization, meaning we can break any composite number
down into multiplication of prime numbers.  For example, $27 = 3 \times 3
\times 3$ and $1015 = 5 \times 7 \times 29$.  Lastly, it should be clear that
all of these prime factorizations have the form $a^x b^y c^z \dots$ where
$a,b,c$ are prime numbers and $x, y, z$ are natural numbers.  For example,
$235,824 = 2 \times 2 \times 2 \times 2 \times 3 \times 17 \times 17 \times 17
= 2^4 3^1 17^3$.

Now for our mapping, pick any two prime numbers $a$ and $b$.  Then we map the
rational number $\frac{p}{q}$ to the number $a^p \times b^q$.  Every rational
number gets mapped to a unique natural numbers.  We can be sure of this because
if we somehow have two rational numbers $\frac{p}{q}, \frac{r}{s}$ such that
$a^p b^q = a^r b^s$, it _must_ be the case that $p = r$ and $q = s$, otherwise
we'd have two different prime factorizations of the same number, which we know
is false.

Now our mapping is _injective_ but not bijective.  Every rational number is
mapped to a unique natural number, but there are natural numbers that are
unpaired.  In particular, any number that doesn't have $a$ or $b$ as prime
factors is not being mapped to.  But since the mapping is injective, we know
that the subset of $\nats$ that are mapped to $\rats$ is infinite, and we have
already stated earlier than any infinite subset of $\nats$ has cardinality
$\aleph_0$, thus we have already proven that $\card{\rats} = \aleph_0$.

As a corollary, this also proves that the natural numbers have the same
cardinality as the set of pairs of natural numbers, since that's essentially
how we treated the rationals.  Further, that proves that $\nats$ as the same
cardinality as the set of triples, quadruples and all n-tuples.

Because the natural numbers are also referred to as the counting numbers, we
often refer to any infinite set with cardinality $\aleph_0$ as **countable**.

## The Diagonal Argument

We have just shown that a lot of sets that _seem_ either bigger or smaller than
$\nats$ actually have the same cardinality.  Now it's finally time to move on
to the diagonal argument itself, which we can now write more precisely as:

_There exists a set $X$, such that $\card{X} > \aleph\s{0} $_

This means that $X$ has the property that no matter how we try, there will
always be at least one element that cannot be assigned a unique natural number.  $X$ would then be an
**uncountable** set.  We're not attempting to say exactly how many more
elements $X$ will have (although based on our previous proofs, the number of
extra elements could neither be finite nor countable), just the fact that it
does have more.

Let's think back to Hilbert's hotel.  With the example of the infinitely large
bus, it should now be clear that we were assuming that the bus contained a
countably infinite set of passengers.  However, if it contained an uncountable
set of passengers, no clever trick would be able to help the hotel manager.  No
possible re-ordering or reassigning of rooms would be able to make enough room
for all the passengers.

So what is this set $X$ that has more elements than natural numbers?  It
is $\reals$, the set of real numbers.  Recall that the real numbers include all
the rational numbers, but also irrational numbers like $\sqrt{2}$ and $\pi$.
So finally, we can write the diagonal argument in its true form:

**Cantor's Theorem:**

$$
\card{\reals} > \card{\nats}
$$

_proof:_ We are going to do what is called a proof by contradiction.  Basically
this means we are going to assume the opposite of our theorem, that
$\card{\reals} = \card{\nats}$, and then show that this assumption leads to a
logical contradiction.  If the negation of our theorem causes a contradiction,
the only possible conclusion is that our original theorem must be true.

If we are making the assumption that $\card{\reals} = \card{\nats}$, then it
follows that a bijective mapping exists between them.  If we want to prove that
this assumption is wrong, then we have to show that every possible mapping
between $\nats$ and $\reals$ is not bijective.  Now obviously we are not going
to be able to simply write out every possible mapping and show one-by-one that
it is not a bijection.  Instead, our argument has to be completely generalized
to simultaneously talk about every such mapping all at once.  This means at no
point in our proof can we say anything that isn't true of all mappings between
the two sets, and everything that we do say must be in the context of a
completely generalized mapping.


What would a generalized bijective mapping between $\reals$ and $\nats$ look
like?  We can simply start writing out all of the natural numbers, and then
assigning a unique arbitrary real number to each one:

$$
\begin{array}{ccc}
\nats & \iff & \reals \\
\hline
1 & \iff & r_1 \\
2 & \iff & r_2 \\
3 & \iff & r_3 \\
...
\end{array}
$$

We are not specifying which real number in particular we are pairing with 1,
only that once we've chosen a number, we don't use it again in any other
pairing.  Now remember, we are assuming for now that even if we can't describe
it with a definite equation, we are _assuming_ that this mapping is bijective
and that every real number appears somewhere on the list.

We can now make our mapping a bit more detailed.  First, we're actually going
to change the proof a little bit.  Instead of using all the real numbers in our
map, we're actually only going to use the numbers between 0 and 1.  Of course,
if we prove that $\card{(0,1)} > \card{\nats}$, we also do it for all of $\reals$,
but making this restriction simply makes the proof easier to follow.

Now, recall that every real number can be written as an infinite decimal
sequence.  For a number like $\frac{1}{4}$, we can write it as $0.2500000...$.
For irrational numbers like $\pi = 3.1415926...$, this sequence never repeats
or terminates, but nevertheless the sequence does exist and is unique.
Therefore for our unique, but arbitrary real number $r_1$, we can write it not
as a single symbol, but as a sequence of symbols, one for each digit:

$$
r_1 = 0.r_1^1, r_1^2, r_1^3, \dots
$$

We are basically assigning every individual digit of the decimal sequence a
placeholder.  For example if we decided $r_1 = \frac{1}{4} = 0.25000...$, then
$r_1^1 = 2$, $r_1^2 = 5$, and $r_1^n = 0$ for all $n > 2$.  The whole point of
this is to think of real numbers not such much as numbers but rather more like
infinite sequences of digits.  Now our generalized bijective mapping can be
written as

$$
\begin{array} {ccc}
1 &\iff & r_1^1 ~ r_1^2 ~ r_1^3 \dots \\
2 &\iff & r_2^1 ~ r_2^2 ~ r_2^3 \dots \\
3 &\iff & r_3^1 ~ r_3^2 ~ r_3^3 \dots \\
\dots
\end{array}
$$

Notice we're not even mentioning the initial $0.$ anymore, since we know it's
always going to be the same.  Again, we are _assuming_ that this mapping
is bijective, even though we are never specifying exactly what the mapping is. 

Now it's time to uncover our contradiction.  We are going to show that when you have a mapping like this, there is always going to be at least one real that didn't fit in the mapping.  We will do that by walking through the mapping and constructing a new real number along the way.

So we are going to build, digit by digit, a real number $r_x$, such that $r_x \neq r_n$ for all natural numbers $n$.

We start by looking at $r_1^1$, the first digit of the real number mapped to 1.
Since we haven't chosen a specific real number, we don't know what digit this
is, but we do know it can only be one of 10 possible values.  We will now
choose $r_1^1 + 1$ as $r_x^1$, unless it is 9, in which case we choose $0$.
For example, if $r_1 =\frac{1}{4}$, recall that $r_{1_1} = 2$, so we could
choose $r_x^1 = 3$.

Notice again we have not specifically said what digit we're choosing, but what
we do know is that since we have enforced that it doesn't match the first digit
of $r_1$, that $r_x$ cannot be the same number as $r_1$.

Now we move on to $r_2$ and do the same thing, except this time we look at the
_second_ digit of $r_2$ to choose $r_x^2$.  Now just as the first digit
of $r_x$ didn't match the first digit of $r_1$, the second digit of $r_x$
doesn't match the second digit of $r_2$.  So $r_x$ cannot be either $r_1$ or
$r_2$, because it differs from each of them in at least a single digit.

We continue to do the same with the 3rd digit of $r_3$, the 4th digit of $r_4$, and so on.  Each time we are forcing $r_x$ to be a different number.

We can generalize this to say that for the $n$th digit of $r_x$, it must be different from the $n$th digit of $r_{n}$.  When we've done this for the entire mapping, we end up with $r_x$, a real number that is guaranteed to be different from every real number in the mapping because it differs from each number by at least one digit.  But our assumption was that every real number was already in the mapping, yet here we are with a number that clearly does not fit, a contradiction!  Therefore the only conclusion is that our assumption is wrong and the mapping is not bijective.

Let's take a look at a concrete example.  Suppose our bijective mapping starts out as such:

$$
\begin{array} {ccl}
1 &\iff &  \frac{1}{4} \\
2 &\iff &  \pi - 3 (0.1415926...)\\
3 &\iff &  0.10987654321010987654321... \\
4 &\iff &  0.0101010101...\\
...
\end{array}
$$

We're only writing the first 4 elements here, but remember we're making the assumption that this mapping is bijective.  Rewriting this in our sequence notation we get:

$$
\begin{array} {ccl}
1 &\iff & \fbox{$\mathbf{2}$},5,0,0,0,0,0,... \\
2 &\iff & 1,\fbox{$\mathbf{4}$},1,5,9,2,6,...\\
3 &\iff & 1,0,\fbox{$\mathbf{9}$},8,7,6,5,... \\
4 &\iff & 0,1,0,\fbox{$\mathbf{1}$},0,1,0,...\\
...
\end{array}
$$

Notice that the nth digit of the nth number is highlighted.  These are the
digits we look at when creating $r_x$.  Here we have the sequence (2, 4, 9, 1).
So following the rules above, we generate a sequence $(3, 5, 0, 2)$, so we know
for this particular mapping, the number we're building starts as $0.3502$.

Clearly this number is not mapped to any of the natural numbers $1$ through
$4$, but maybe $5$ maps to a real number that starts with $0.3502$.  This is
why when we look at the 5th number, we choose a digit different from the 5th
digit, so that even if $r_5$ starts with $0.3502$, we can still conclude that
$r_5$ is not $r_x$ since we'll know that $r_x^5$ and $r_5^5$ aren't equal. 

It should be clear that it never really mattered that we chose $\frac{1}{4}$ as
our mapping to 1 and so forth.  If we had chosen something different for our
mapping, the actual value of $r_x$ would be different, but the procedure for
building it would be the same no matter what mapping we started out with.

Since at no point in the proof did we ever actually write any detail about
which mapping in particular we were talking about, the above argument applies
to _all_ possible bijective mappings between $\nats$ and $\reals$.  But that
means that any possible bijective mapping results in a conradiction and thus is
not actually a bijective mapping.  Thus we have proven that no bijective
mapping exists between $\nats$ and $\reals$ and furthermore that any possible
mapping ends up with unpaired elements of $\reals$.  Therefore $\card{\reals} >
\card{\nats}$.  **QED** 

## More Uncountable Sets

We've just shown that comparing $\reals$ and $\nats$ yields two different
infinite cardinalities.  Are there any more?  Is there some set whose
cardinality is even larger than $\card{\reals}$?  In fact, there are infinitely
many!  I'll show a simple way how to build ever larger infinite sets.  But
first, we need to look at another set whose cardinality equals $|\reals|$: the
**Power Set** of $\nats$, abbreviated $\mathcal{P}(\nats)$.  This is defined as
the set of all possible subsets of $\nats$.  

For example, if we have $A = \\{a, b, c\\}$, $\mathcal{P}(A)$ is the set that contains these sets:

$$
\begin{array}{ c c c }
 \{a\} & \{b\} & \{c\} \\ 
 \{a, b\} & \{a, c\} & \{b, c\} \\
 \emptyset & A
\end{array}
$$

So $\mathcal{P}(A)$ contains every possible combination of elements in $A$,
along with $\emptyset$ and $A$ itself.  Notice that $\card{A} = 3$ and
$\card{\mathcal{P}(A)} = 2^3 = 8$.  In general for any finite set $S$ with
cardinality $n$, $\card{\mathcal{P}(S)} = 2^n$.  If we take
$\mathcal{P}(\mathcal{P}(A))$, we end up with a set with 256 members, a few
examples are:

$$
\{ \{a\}, \{a, b\}\}, \{\emptyset, \{a, c\}, \{b, c\}\}, \{A\}
$$

So now we have a set containing sets of subsets of $A$.  

Of course, we are perfectly able to take the powerset of infinite sets as well
(in fact, the \emph{Powerset Axiom} is an axiom of set theory that says we can
take the powerset of \emph{any} set).  So $\mathcal{P}(\nats)$ is the set
containing every possible subset of natural numbers, some examples being:

$$
\{1, 2, 3\}, \{2, 3, 5, 7, 11, 13, 17, ...\}, \{4, 44, 444, 4444, ...\}
$$



_For any set $S$, $\card{\mathcal{P}(S)} > \card{S}$_

The proof is another diagonal argument, but this time it's a bit more
complicated so we won't go into it here.  But this theorem means that
$\mathcal{P}(\reals)$ is a set that is even more uncountable than $\reals$
itself.  And every subsequent application of the powerset function yields an
even bigger and bigger set.


## The Continuum Hypothesis


One question we might ask ourselves is "is there a set $S$ such that
$\card{\nats} < \card{S} < \card{\reals}$?"  If you think about it, Cantor's
proof shows that $\reals$ is larger than $\nats$, but it never makes the claim
that $\card{\reals}$ is the very next size up from $\card{\nats}$.  Perhaps there could
be a set somewhere whose cardinality fits right in between, or maybe there's
even an infinite number of cardinals in between (maybe even uncountably
infinite!).

Cantor too asked the same question and posed it as a statement known as the **Continuum Hypothesis** (abbreviated CH):

$$
\card{\reals} = \aleph_1
$$

Cantor found himself unable to prove that CH was either true or false.
Actually everybody who tried to come up with an answer one way or another came
up short.  Mathematicians became frustrated and fascinated with their inability
to prove or disprove what seemed like a relatively simple question.  In 1900,
the mathematician David Hilbert compiled a list of 23 of the most important
unsolved mathematical problems at the time, and CH was first on the list.

### The Briefest Introduction to ZFC Possible

This section can be skipped, but it might help make a little more sense of what comes afterwards.

Cantor's diagonal proof kick-started the field of Set Theory, but it was the
work of quite a few mathematicians to get it well established.  One of the
earliest issues in set theory is known as Russell's paradox, named after it's
discoverer, Bertrand Russell.  The paradox arises when attempting to
conceptualize sets of things that satisfy some property.  For example, can we
create the set of all even integers?  Seems like a reasonable proposal, and
most agreed that if we assumed that $\nats$ exists, then the set of even
numbers seems perfectly fine.  So too would be the set of all odd numbers, and
even sets whose members we can't easily describe like the set of all prime
numbers or the set of all atoms in the universe.  

So set theory was created with a notion of "comprehension", that if there was
some property $\phi(x)$, which reads as "$x$ is a (something)" or "$x$ has
(some distinguishing property)", then we can create the set of all the $x$'s
such that $\phi(x)$ was true.  This can be written as $\\{x : \phi(x)\\}$, or
more literally as "the set of $x$'s such that $\phi(x)$ is true."

It turns out though that this form of comprehension leads to a paradox.  What
if we try to create the set of all sets that don't contain themselves.  In
other words we want to create the set $A = \\{x : x \not \in x\\}$.  But since
$A$ itself is a set, does $A$ contain itself?  Well if $A$ does contain itself,
then it doesn't satisfy the property $x \not \in x$, so that means $A$ can't
contain itself.  But then that would mean the property _is_ satisfied, and
we're stuck in an endless cycle of $A$ both containing and not containing
itself.

Thus people realized that so-called "unrestricted comprehension" was not a
valid rule for creating sets, and this led to mathematicians deciding that a
more precise set of rules needed to be written down.  These rules became what
is now call Zermelo-Fraenkel set theory with the axiom of Choice (ZFC).  ZFC is
a set of about 9 rules called "axiom", and serves as not only the foundation
for modern set theory, but most of mathematics in general.  Among its axioms is
a more restricted form of comprehension that says you are allowed to form any
set from some property $\phi(x)$, except you must start with some already known
set and only select from that set elements that satisfy $\phi(x)$.  Russell's
paradox is avoided because it is simply not possible in ZFC to create a set of
all sets not containing themselves.

### The Independance of CH from ZFC

It took sixty-three years and two different mathematicians to finally answer
the question of CH, although the answer opened far more doors than it closed.  In
1931 the logician Kurt Godel proved that CH could not be proven false.  This
doesn't mean he proved it was true, he showed that if CH was added as an axiom to
ZFC, there are no logical contradictions.  In 1963, Paul Cohen proved that CH
could not be proven true!  The opposite of what Godel proved, Cohen showed that
if you take the opposite of CH as an axiom (i.e. you assume that $\card{\reals} > \aleph_1$),
 there are also no contradictions.  What this means is that CH is
_independent_ of the other axioms of ZFC.  ZFC can neither prove the
statement true or false.

So what does this mean?  Is CH unprovable?  Can it be both true and false?
Both of these assertions seem to make no sense.  CH is a statement about the
existence of sets, surely they either do or do not exist.  This brings us back
to the discussion about intuition.  Just as with the axiom of infinity, CH is
something we cannot prove, but unlike infinity there is no clear intuition
about its truth.

