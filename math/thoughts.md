---
layout: math
---

# Thoughts on Various Topics


## On cofinality

The cofinality of a set $A$ is the size of the smallest possible unbounded
subset of $A$.  For example.  $\text{cf}(\omega) = \omega$ since the only way
you could have an unbounded set in $\omega$ is if it is infinite, and all
infinite subsets have size $\aleph_0$.

Similarly, a function $f: \lambda \to \kappa$ is cofinal in $\kappa$ if is
unbounded in $\kappa$.  Thus the smallest such $f$ has size $cf(\kappa)$.  This
also means that if $\lambda < cf(\kappa)$, f is bounded by some ordinal $\alpha
\in \kappa$

If $cf(\kappa) = \kappa$, then $\kappa$ is _regular_.  Otherwise it is _singular_.

For example, the cofinality of $\aleph_1$ must be $\aleph_1$, since a subset of size
$\aleph_0$ can not possibly be unbounded.  To show this, use this set $B$ to
partition $\aleph_1$, using each element of $B$ as an index and assigning every
element of $\aleph_1$ to the first index it is larger than.  At least one of
these subsets must be of size $\aleph_1$.  This subset must also be the
"largest" such subset, for if not, it would mean that an uncountable number of
elements preceded a countable ordinal, a contradiction.  Thus $B$ cannot be
unbounded and $\aleph_1$ is regular.

Many limit cardinals are singular.  $\aleph\s{\omega}$ is singular since
$\\{\aleph_0, \aleph_1, \aleph_2, \dots\\}$ is a sequence of size $\aleph_0$
unbounded in $\aleph\s{\omega}$.  $\aleph\s{\omega_1\}$ is the smallest
singular cardinal with uncountable cofinality.

$\aleph_0$ is the only regular limit cardinal provable in ZFC.  The first
uncountable regular limit cardinal is called a _weakly inaccessable cardinal_,
as it is not provable in ZFC.  A strongly inaccessable (just called an
_inaccessable_ cardinal) also has the property that $\lambda < \kappa \to
2^\lambda < \kappa$.  The two are equivalent under GCH.

If GCH is false, it can be possible that $2^{\aleph_0} > \aleph\s{\omega_1}$

## On fixed points of the aleph function

The aleph function is a class function $f: \text{Ord} \to \text{Ord}$ where
$f(\alpha) = \aleph\s{\alpha}$.  A fixed point for a function $f$ is some $x$ such
that $f(x) = x$.  It is a well known theorem that for any normal function
(strictly-increasing and continuous) that it must contain fixed points.

The aleph function is normal:

It is strictly increasing.  Consider $\alpha < \beta$.  Obviously $\aleph_\alpha < \aleph_\beta$

It is also continuous.  Consider a limit ordinal $\alpha$.  We must show that
$\aleph\s{\alpha} = \text{sup}\\{\aleph_\beta : \beta < \alpha\\}$.  However that is
the very definition of a limit cardinal, which we know is the case for
$\aleph_\alpha$.

Therefore the aleph function is normal and contains fixed points.  Let's
consider the smallest such point, what do we know about it?

It must be a limit cardinal.  Suppose $\alpha$ was a successor cardinal for
some $\lambda$.  This means you'd have both $\lambda^+ = \alpha$ and $\lambda^+
= \aleph_\alpha$.  Then $f(\lambda)^+ = \aleph_\lambda^+$.  Because strictly
increasing, $f(\lambda) < f(\lambda^+)$, so $f(\lambda) < f(\alpha) = \alpha$.
Also and we must have that $f(\lambda) \geq \lambda$.  But that means that
$f(\lambda) = \lambda$, contradicting the fact that $\alpha$ is the smallest
such fixed point.

**All weakly innaccessable cardinals are fixed points.** Suppose not, that
means that $\kappa = \aleph\s{\lambda}$ for some $\lambda \neq \kappa$.  We
cannot have $\lambda < \kappa$, since otherwise $\\{\aleph\s{\gamma} : \gamma < \lambda\\}$ 
is cofinal in $\kappa$, making $\kappa$ singular, a contradiction.
It also cannot be the case that $\lambda > \kappa$, since then we'd have
$\lambda > \kappa \to \aleph\s{\lambda} > \aleph\s{\kappa}$, but that would
mean $\kappa > \aleph\s{\kappa}$, another contradiction.  So it must be the case
that $\kappa = \aleph\s{\kappa}$. $\square$

**The first fixed point is definable in ZFC**.  The limit of the sequence
$\\{\aleph_0, \aleph\s{\aleph_0}, \aleph\s{\aleph\s{\aleph_0}}, \dots\\}$ is
the smallest fixed point and is clearly definable using replacement.  So it is
not the case that all fixed points are weakly inaccessable. $\square$

## On Stationary Sets

### Club sets

A subset of a regular uncountable cardinal $\kappa$ is closed-unbounded if it
is unbounded in $\kappa$ and contains all its limit points.

club sets are generally considered large, since they must be of size $\kappa$
(since $\kappa$ is regular).  A club does not necessarily contain all the limit
points of $\kappa$.  For example, consider $A = \aleph_4 - \aleph_3$ as a
subset of $\aleph_4$.  Clearly $A$ is a club set but it doesn't contain any of
the limit ordinals found in $\aleph_3$.  Nevertheless, it is still of size
$\aleph_4$.

A set is stationary if it intersects every club.  While all club sets are
stationary, the opposite is not necessarily true.  For example, take $\aleph_4 - \aleph_3$, 
which is club, but now delete $\aleph_3 + \omega$.  It's still
unbounded and stationary, but no longer closed since it doesn't contain the
limit of $\aleph_3 + n : n < \omega$.  


In fact, take any club set and delete 1 limit ordinal, and the result is a
stationary set that is not club.  Actually $< \kappa$ limit ordinals can be
removed  without risking the  set being unbounded.

**Every stationary set is unbounded.** _Proof:_ suppose a stationary set $S$ was
bounded in $\kappa$ and let $\lambda < \kappa$ be the limit.  Because $\kappa$
is regular, it must be the case that $T = \\{\mu : \lambda < \mu < \kappa\\}$
contains $\kappa$ elements.  Thus $T$ is club and does not intersect $S$, a
contradiction. $\square$

As a corollary, every unbounded subset of $\kappa$ is size $\kappa$ via a similar argument.

Thus we can say that any bounded subset of $\kappa$ is "very small", any unbounded subset is "large", and any stationary set is "sort of large".

The reason why it's important that $\kappa$ be regular is because singular
cardinals have trivial club and stationary sets.  A singular cardinal contains
no stationary sets.  For example, consider $\aleph_\omega$.  It cofinality is
$\omega$ so we know that's the size of the smallest unbounded set.

### Solovay's theorem

This theorem states that every stationary subset of $\kappa$  can be partitioned into $\kappa$ stationary sets.  

I think this follows from the idea that we know we can split it into $\kappa$
disjoint subsets.  We just need to prove that each subset is stationary.  This
also means we need to be careful about how we split up $S$.  I think a scheme
where we "unzip" $S$ into two stationary sets.  If in a sense we assign every
other limit ordinal to $S_1$ and $S_2$, both sets are unbounded and stationary.
If we generalize this idea to $\kappa$ partitions, I think it works.  This
might be what people are talking about with an Ulam matrix.  NOPE

## Proving separation from replacement

\emph{Axiom of Replacement}: Given a class function $F(x)$ and a set $A$, we can construct a set $B$ such that $B$ is the range of $A$ under $F$.\newline

\emph{Axiom of Separation}: Given a predicate $P(x)$ and a set $A$, we can create $\{a \in A: P(a)\}$.\newline

The axiom of separation can be proven from replacement as such:  Given a predicate $P(x)$, we can create the formula $F(x,y) = x = \{y\} \land P(x) \lor y = \emptyset \land \neg P(x)$.  Clearly $F$ is a function since for any $x$, either $P(x)$ and thus $y$ must equal $\{x\}$, or $\neg P(x)$ and $y$ must equal $\emptyset$.  Thus through replacement we can create a set $B$ which contains all the $\{x\}$ where $P(x)$, along with possibly the empty set.  Through the axiom on union we take $C = \bigcup B$ and now C contains exactly the $x$ such that $P(x)$.  THIS ASSUMES THAT $\emptyset$ and $\{x\}$ exist!!\newline

Let's look at replacement more closely:


## Extensions of ZFC by adding a constant

The language of ZFC merely contains the single relation symbol $\in$.  We can use the technique of adding constants to the language.

Assume $Con(\text{ZFC})$ and let $\mod{A}$ be a countable model.  We can add an new constant $c$ to the language along with some statement regarding each element of $\mod{A}$.  For example, we can add, for each 
