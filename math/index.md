---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
---

# Dan's Math Notes

This is a place for me to dump all of my thoughts and ideas on various math
topics.  I'm mostly interested in set theory so that's what you'll find here.

**Disclaimer** : _I am not a professional mathematician nor a student.  I have a
background in CS and I'm entirely self-taught when it comes to pretty much
everything here.  Please take everything here with a grain of salt, and even
though I've been studying this stuff for a while, I'm probably wrong in more
than one place._

## Casual Guides

These are intended for people with little or no knowledge of higher math, but
are interested.  Light on proofs and heavier on intuition and motivation.

[A Casual Guide to ZFC](zfc)

[A Casual Guide to Cantor's Diagonal Proof](cantor.html)


## Set Theory

[Set Theory Problems](set_theory_problems.html)

## Model Theory

[Notes on Model Theory](model_theory_notes.html)

[Model Theory Problems](model_theory_problems.html)

## Other

[Graham's Number](grahams_number.html)

[Large Countable Ordinals](large_countable_ordinals.html)

[Thoughts on Various Math Topics](thoughts.html)
