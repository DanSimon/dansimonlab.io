---
layout: math
---



# Model Theory Definitions

## The Basics

### Languages

#### Atomic Formulas and Sentences

A **term** is:

* A constant symbol $c$
* A variable $x_n$
* A function symbol $F(t_1, t_2, \dots, t_n)$ where all the $t_n$ are terms

The **atomic formulas** of a languages are of the form:

* $t_1 \equiv t_2$
* $R(t_1, t_2, \dots, t_n)$ for every relational symbol $R$

**Formulas** are:

* atomic formulas
* $\phi \lor \psi$
* $ \neg \phi$
* $\exists x \phi$

A **sentence** is a formula with no free variables.  An **atomic sentence** is
an atomic formula with no variables.

### Models

$\mathfrak{A}'$ is a **submodel** of $\mathfrak{A}$ if

1.  $A' \subset A$
2.  All n-ary relational symbol interpretations are restricted to $A^n$
3.  All n-ary functional symbol interpretations are restricted to $A^n$
4.  All the constant interpretations are the same

Conversely, $\mathfrak{A}$ is an **extension** of $\mathfrak{A}'$ if $\mathfrak{A}'$ is a submodel of $\mathfrak{A}$.

$\mathfrak{A}$ is **isomorphic** to $\mathfrak{B}$ if there's a bijection from $f : A \to B$ with 

1.  $R_A(a,b,c) \iff R_B(f(a), f(b), f(c))$
2.  $f(F_A(a, b, c)) = F_B(f(a), f(b), f(c))$
3.  $f(c_A) = c_B$

$\mathfrak{A}$ has an **isomorphic embedding** into $\mathfrak{B}$ if there is a submodel $\mathfrak{C}$ of $\mathfrak{B}$ such that $\mathfrak{A} \cong \mathfrak{C}$

A **REDUCT** $\mathfrak{B}$ of $\mathfrak{A}$ is the natural reduction of a
model of an extension of a language $L$ to $L$ itself.  For example.  If we
have some language $L$ with a model $\mathfrak{A}$, we can extends it to a
languange $L'$ by adding a new constant symbol.  We can then create an
extension $\mathfrak{B}$ of $\mathfrak{A}$ by adding some element of $A$ as the
interpretation.  $\mathfrak{A}$ is a reduct of $\mathfrak{B}$.

### Consistent Theories

A Theory is :

* **Closed** if $T \models \phi \to \phi \in T$
* **Consistent** if for all sentences $\phi$ there is no proof of $\phi \land \neg \phi$
* **Complete** if for all sentences $\phi$ either $T \models \phi$ or $T \models \neg \phi$


If we have two theories $T_1$ $T_2$ with $T_1 \subset T_2$, then any model of $T_2$ is also a model of $T_1$.  

### Witnesses

Let $\mathcal{T}$ be a set of sentences of $\lan{L}$ and let $C$ be a set of constant symbols in $\lan{L}$.  $C$ is a set of witnesses for $T$ if for every formula $\phi$ with at most one free variable, there is a $c \in C$ such that:

$$
T \vdash (\exists x)\phi \to \phi(c)
$$

#### Diagram of $\mathfrak{A}$

given some model $\mathfrak{A}$ of a language $L$, create a new languange $L_A$
by adding a new constant for each element of $A$.  Now create a new model
$\mod{A}\s{A}$ extending $\mod{A}$ by interpreting each constant symbol by
the element of $A$ it came from.The **diagram of $\mathfrak{A}$** ($\Delta_A$) is
the set of atomic sentences or negation of atomic sentences of $\lan{L}\s{A}$
that hold in $\mathfrak{A}\s{A}$.

This basically means the diagram of $\mod{A}$ is essentially a model that
captures all of the structure around its elements.  If some atomic formula
happens to hold (or not hold) for elements in $A$, then $\Delta_A$ captures that as a
sentence in $\mod{A}\s{A}$.

For example suppose we have some language $\lan{L}$ with a relational symbol
$R(x,y)$.  Then in a model $\mod{A}$ of $\lan{L}$ we have two elements $a_1,
a_2$, such that $R(a_1,a_2)$ is true.  Then $\lan{L}\s{A}$ will have two
constants $c_1, c_2$ and the interpretation of those constants in
$\mod{A}\s{A}$ will just be $a_1, a_2$.  $\Delta_A$ will also contain the
sentence $R(c_1,c_2)$.

It is clear that every model of $\Delta_A$ must contain an isomorphic embedding
of $\mod{A}\s{A}$.  It could be the case that some model $\mod{B}$ of $\Delta_A$
is not isomorphic to $\mod{A}\s{A}$ and could have elements that are not the
interpretations of any constants, but it must contain some submodel whose only
elements are constants, in which case that it must be isomorphic. 

