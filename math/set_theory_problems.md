---
layout: math
---

# Set Theory Problems

from Kunen's book

## Chapter 1

### 1.9.39

**Assume that $R$ is well-founded and set-like on $A$.  Then rank$\s{A,R}$ is 1-1 _iff_ $R \star$ is a total order of $A$.**

Recall that 

* the rank $r$ of a relation on a class is $r(y) = \\{r(x): x \in y \downarrow\\}$.  
* $R \star$ is the transitive closure of $R$.  
* $x \downarrow$ is the class of all elements $y \in A$ such that $y R x$
* "Set-like" means if $A$ is a class, for all $x \in A$, $x \downarrow$ is a set
* A total order is 
	* Transitive
	* Irreflexive $\forall x : x \not R x$
	* satisfies trichotomy : all elements are comparable

If we assume the former, then no two elements of $A$ can have the same rank.
This means for any $x,y \in A$, either $r(x) < r(y)$, $r(x) > r(y)$ or $r(x) =r(y)$.  
Of course in the last case it must be that $x = y$, so now let's
consider the case that $r(x) < r(y)$.  We must show that $x < y$.

Because $R$ is well-founded, let $z$ be the $r\s{<}$ minimal element that is
between $r(x)$ and $r(y)$.  This $z$ must be unique since it has rank $r(x) +
1$.  The same applies to all of the elements of rank between $r(x)$ and $r(y)$,
so $R$ is a total-order.  Since a total order is transitive, then $R \star = R$
and is thus also a total order of $A$.

If $R \star$ is a total order on $A$, then rank$\s{A,R \star}$ is 1-1, and since $R \subseteq R \star$, $R$ is also 1-1

### 1.15.28

**Fix $P \subseteq A$, and assume that there is a binary relation $R$ on $A$
that well-orders $A$ and that is definable of $\mod{A}$ with parameters in $P$.
Let $\mod{H}(\mod{A}, P)$ be the set of elements of $A$ that are definable with
parameters in $P$.  Then $\mod{H}(\mod{A}, P)$ is an elementary submodel of
$\mod{A}$.**

It is hinted that the _Tarski-Vaught criterion_ can help, which is:

Let $\mod{A} \subseteq \mod{B}$ be models of $\lan{L}$, then the following are equivalent

* $\mod{A} \preccurlyeq \mod{B}$
* For all existential formulas $\phi(\overrightarrow{x}) $ of $ \lan{L}$ (of the
  	form $\exists y \psi(\overrightarrow{x}, y))$, and all $\overrightarrow{a} \in A$, 
	if $\mod{B} \models \phi[\vec{a}]$, then there is some $b \in A$
	such that $B \models \psi[\vec{a}, b]$.


So if we can prove the second point, then we are done

Let $\phi$ be such an existential formula.  Fix some $\vec{a} \in \mod{H}$.  If
$\mod{A} \models \phi[\vec{a}]$ then there must be a $b \in \mod{A}$ such that
$\mod{A} \models \psi[\vec{a}, b]$.  

Now, we need to show that $b$ is definable with parameters in $P$, which
amounts to saying that each element of $\vec{a}$ must also be definable with
parameters in $P$, since then $\psi$ can define $b$.  For each $a_n \in \vec{a}$, let $\theta_n(x, \vec{p})$ be a formula that defines $a_n$ with parameters in $P$.  Then the combined formula:

$$
\exists x_1 \dots \exists x_n \theta_1(x_1, \vec{p_1}) \land \theta_n(x_n, \vec{p_n}) \land \psi(b, x_1 \dots x_n)
$$

defines the set of $b$'s that satisfy $\psi$ defined with parameters in $P$.
There could be several such elements, so we use $R$ to select the $R$-minimal
one, defining a single element with parameters in $P$.

Then it is also the case that $b$ is
definable in $\mod{A}$ with parameters in $P$, so $b \in \mod{H}$.  Thus the
criterion is satisfied and $\mod{H} \preccurlyeq \mod{A}$.

### 1.16.7

**Let $\gamma > \omega_1$ be a limit ordinal.  Prove that there is a countable
transitive $M$ and ordinals $\alpha, \beta \in M$ such that $M \equiv
R(\gamma)$ and $(\alpha \approx \beta)^M$ is false but 
$(\alpha \approx \beta)^{R(\gamma)}$ is true.**


