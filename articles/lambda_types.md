---
layout: math
---


# Lambdas as Types

Scala is often touted as having an exceptionally powerful type system.  This is
great for letting developers encode business logic into types and
having more confidence about the correctness of their code.  But it also means
you can do some really crazy stuff in Scala that you can't do in most other
languages.

Here I will demonstrate that it's possible to write and execute complete
programs in Scala's type system.  In fact we'll take it one step further by
implementing untyped lambda calculus, complete with an interpreter, all in the
type system.  This means we'll be defining the data structures to represent an
lambda calculus expressions and an interpreter to evaluate expressions all as
types.  The Scala code we'll write will never actually run, the whole thing
happens at compile-time.

While being familiar with Scala won't hurt, it shouldn't be required to follow
along, since we won't actually be writing much Scala besides a whole bunch of
type aliases.  I'll also be going over all the important features of Scala's
type system used to accomplish what we're doing.

Some of this is based off this great guide on [Typelevel Programming in
Scala](https://apocalisp.wordpress.com/2010/06/08/type-level-programming-in-scala/)
by Mark Harrah.

**For the impatient:** Here's the github repo.  I've annotated the code enough
that those who are more famaliar with Scala and/or typelevel programming can
probably just skip everything here and go straight to the source.

## So What Exactly Are We Doing?

Normally we think of a type as a descriptor for a variable that indicates how
we sould interpret its data:

```scala
val an_integer: Int = 4
val a_string: String = "hello"

val another_integer : String = 5 //won't compile

//a type alias
type BunchOfChars = String

//type alias with a parameter
type BunchOfThings[T] = Array[T]

//this works because the compiler proves Array[Int] and BunchOfThings[Int] are
//really the same type
val bunchOfInts : BunchOfThings[Int] = Array[Int](1, 3, 5, 7)

//a compile-time type equality check
implicitly[String =:= BunchOfChars]

//this will fail to compile
implicitly[Int =:= Array[Int]]

//a compile-time subtype check
class Foo
class Bar extends Foo

implicitly[Bar <:< Foo]
```

But we can also think of a language's type system as an independant
computational entity.  When code is being compiled, the type checker must often
do complex type transformations and analyses in order to prove that two types
are equal or that one type is a subtype of another.  We'll be taking advantage
of this machinery to encode actual computation in the type system.  If we
create a set of types to represent values and functions in lambda calculus, and
a set of type aliases that transform properly constructed expressions, then we
can get the type checker to run our program when it attempts to collapse the
type aliases down to concrete types.

## What is Lambda Calculus

In a nutshell, (untyped) lambda calculus (LC) is the simplest possible programming
languange that is still sufficiently powerful to perform general computation.
It was designed by Alonzo Church in the 1920's, years before any mechanical or
electrical computer actually existed.  Yet it has remained a cornerstone of
computer science ever since.

LC is literally a logician's take on programming, so it is defined more like 
formal logic than a programming language.  A LC "program" is called an
_expression_ which is made up of _terms_.  A term is defined inductively as such:

* **Variables** $x$, $y$, $z$, etc are terms.  Relative to regular programming languages, LC variables act both as variables and constants.  So "$x$" by itself is a valid LC expression.
* if $\tau$ is a term and $x$ is a variable, then the **lambda abstraction**
  $\lambda x. \tau$ is a term.  This is like defining an anonymous function
  with a single parameter $x$ that returns the expression $\tau$ (which may
  reference $x$).  This is also referred to as _binding_ the variable $x$.
* if $\tau$ and $\phi$ are terms, then the **application** $(\tau ~ \phi)$ is a term.  This is like calling the function $\tau$ with parameter $\phi$.  Notice that $\tau$ may not actually be a lambda abstraction, in which case the "result" is itself.
* Parentheses can be used to disambiguate, for example $((x \ x) \ x)$ vs $(x \ (x \ x))$.  Expressions are naturally left associative so $((x \ y) \ z)$ can be written as just $x \ y \ z$.

And that's (mostly) it!  There are no numbers, strings, or typed literals of any kind,
not to mention if-statements, loops, or any other structure you'd find in
normal programming languages.  Just abstract variables and functions.  And yet,
LC is _Turing Complete_ (proven in the famous Church-Turing thesis), meaning it
is just as computationally powerful as C, Java, or any other real-world
language.  Of course it lacks any sort of real I/O so without an extension it's
limited to number-crunching algorithms, but still that means you could use
LC to perform eliptic-curve cryptography or do protein-folding simulations.

### Free and Bound Variables

Consider the expression $\lambda x. x$, a function that simply returns whatever
is passed to it, aka the identity function.  Because the lambda uses $x$ as
its parameter, $x$ is called a **bound variable**.  Now consider the expression $\lambda x. y$.  Here
$x$ is still a bound variable, but $y$ has not been bound by any lambda
abstraction and is known as a **free variable**.  In this context we treat $y$
like a constant.  If instead we wrote $\lambda x \lambda y. x$ or $\lambda x
\lambda y. y$, both $x$ and $y$ would be bound.  

Sometimes the same variable can be both free and bound in the same expression.  Consider 

$$
((\lambda x. x) ~ x)
$$

I've added some extra parentheses to make it a little more readable.  This is
an application expression, and the lefthand term is the same identity function
$\lambda x. x$ we wrote before.  But the righthand term is the variable $x$.
Because this $x$ is not part of the lambda abstraction, it is not bound.

### $\alpha$ conversion and $\beta$ reduction

So now that we've described the syntax of a program in LC, how do we actually
run it.  The execution of a LC expression again feels more like mathematics
than programming and involves applying a series of transformations to the expression.  

The first of these transformations is known as **$\alpha$-conversion** and is essentially the process of renaming a bound variable.

$$
\alpha(\lambda x.\tau, y) \to \lambda y.\tau[x := y]
$$

This says that given an expression of the form $\lambda x. \tau$, where $\tau$
is an arbitrary term, if we wish to rename $x$ to $y$ then we change it in the
lambda abstraction and then substitute all bound occurrances of $x$ in $\tau$.

$\alpha$-conversion seems trivial at first, but it is actually crucially
important when evaluating expressions and is not as simple as it looks.
Consider the expression:

$$
\lambda x. \lambda x. x
$$

This is a perfectly valid function that returns the identity function.  Both
functions name their parameter $x$, but we know that the variable $x$ is bound
to the righthand lambda abstraction.  If we wish to $\alpha$-convert the first
$x$ to $y$, we must be careful not to just naively rewrite every $x$ to $y$, so
the correct transformation is $\lambda y. \lambda x. x$ and not $\lambda y.
\lambda x. y$ or $\lambda y. \lambda y. y$.

**$\beta$-reduction** is the process of executing a lambda application.

$$
\beta \big((\lambda x. \tau \  y)\big) \to \tau[x := y]
$$

$\beta$-reduction is applied to an application term $(\psi \ \phi)$ when $\psi$ is a lambda abstraction of the form $\lambda x.\tau$.  

So just like with executing a function call, we replace all occurrances of the parameter $x$
with the argument $y$ in $\tau$.  For example, $((\lambda x ~ x) \ y)$ reduces to just $y$.  

### It's not so simple

At first it seems like evaluating a LC expression is a straightforward and
mechanical operation: just recursively and repeatedly apply $\beta$-reduction
until it's done.  But two glaring issues make correct evaluation much trickier.

The first is called _variable capture_ and occurs when the same variable is
used as both a free and bound variable in the same expression: Consider

$$
\lambda x . \lambda y . ~ x
$$

A function that returns another function.  If, for example, we call it with some variable $z$, we get $(\lambda
x. \lambda y . x \  z)$ which $\beta$-reduces to $\lambda y. z$, a function that always
returns $z$.  But what happens if we call it with $y$ instead:


$$
(\lambda x . \lambda y . x \ \  y) \to \lambda y. y
$$

Now we've instead created the identity function, which is not what we wanted.
Essentially we've hit a name collision.  The $y$ by itself is a _free_
variable which means we're using it more like a named constant rather than an
actual variable.  But by substituting it into an expression where $y$ is
already being used as a _bound_ variable, the substituted $y$ switches from
being free to being bound, hence the name "variable capture".   

To avoid this situation, we must apply $\alpha$-conversion on the bound $y$ before applying the substitution:

$$
(\lambda x . \alpha(\lambda y . x, z) ~ y) \to (\lambda x . \lambda z . x ~ y) \to \lambda z. y
$$

Here we chose to replace the bound $y$ with $z$, since $z$ is a "fresh"
variable not used anywhere in the expression.  The free $y$ is unaffected.
After applying $\beta$-reduction, we end up with $\lambda z. y$, which is not
exactly the same as $\lambda x. y$, but it does exactly the same thing.  The
two expressions are **$\alpha$-equivalent**.

You might think we'll be able to avoid variable capture simply by pre-processing an expression to ensure that all bound variables are fresh, however even that doesn't work.  For example:

$$
\Big((\lambda x. (x \ x)) \ (\lambda x. (x \ x))\Big)
$$

We could rename the second bound $x$ to $y$, giving us

$$
\Big((\lambda x. (x \ x)) \ (\lambda y. (y \ y))\Big)
$$

but when we apply $\beta$-reduction, we then get:

$$
\Big((\lambda x. (x \ x)) \ (\lambda x. (x \ x))\Big)
$$

which again leads to variable capture.  Thus we must ensure freshness every time we're about to apply $\beta$-reduction.


### A Capture-avoiding Substitution Algorithm

It's clear that properly evaluating a LC term is more than just repeatedly
applying $\beta$-reduction.  In order to avoid variable capture, we must
carefully rename and substitute variables in the process.  While there are many
different approaches to this, we'll go with something that is simple and
relatively easy to implement, at the cost of some efficiency.

First, instead of using alphabetic variables $x, y$, etc, we'll use natural
numbers.  For example $\lambda x ~ y$ becomes $\lambda 0 ~ 1$.  We're doing
this so it'll be much easier to programatically generate new variables, just
add one!

To address the variable capture problem, we will simply do a mass
$\alpha$-conversion of all bound variables to avoid any possible naming
collision with a free variable.  The algorithm is as follows, given a terms
$\tau, \phi$ and variable $n$, and we wish to perform the substitution $\tau[n
:= \phi]$:

1.  Given some term $\psi$, define $M(\psi)$ to be the value of its largest free variable.  For example $M(\lambda0. (8 \lambda 11. (2 0))) = 8$.
2.  Let $k$ be $max(M(\tau), M(\phi))$.
3.  Traverse $\tau$, $\alpha$-converting every bound variable by adding $k$ to it.

We don't need to do any alpha conversion in $\phi$ since it is being injected
inside $\tau$ and thus cannot capture any of its variables.

### Expression Normalization

Our above algorithm will allow us to perform capture-avoiding
$\beta$-reduction, but it brings forth our second issue: knowing when to stop
evaluating.

Consider the expression $(x \ y)$.  If we apply $\beta$-reduction, the result
is just $(x \ y)$ again, and it should be obvious that any further iterations
of $\beta$-reduction will yield the same result.  Contrast this with an
expression like 

$$
\Big((\lambda x. \lambda y. \ (x \ y)) \  (\lambda x. \ x) \ z \Big)
$$

which defines a 2-parameter function and then calls it with the first argument
a function and the second a variable.  If we simply do one traversal of the
expression, applying $\beta$-reduction whenever possible, we end up with:

$$
\big((\lambda x. \ x) \ z \big)
$$

But this expression can still be reduced further to just $z$.  So we should
simply loop $\beta$-reduction traversals until we detect that a traversal has
no effect.  Of course there are many expressions that will never terminate, such as 

$$
\lambda x. \ x \ x \x \lambda x. \ x \ x \x
$$

which just keeps growing forever.  But other expressions will terminate reduction but in a non-$\alpha$-equivalent way.  For example, if we use our above algorithm on

$$
\lambda x. x \ x \lambda x. \ x \ x
$$

the alpha conversion step will change this to 

## The Magic of Scala's Type System

So what is it about Scala's type system that will allow us to perform general
computation?  There are primarily three features we'll be employing:
higher-kinded types, type members, and type projection.  We'll cover each
briefly though there's no need to go into any in-depth type theory.

We'll also briefly cover implicits a bit since we'll be using them to write
programs to properly output lambda calculus expressions.

### Higher-kinded Types

Often the posterchild for "powerful" type systems, higher-kinded types (HKT)
essentially give you the ability to write typelevel higher-order functions.  A
typelevel function, often called a _type constructor_, is like a function that
accepts one or more types and returns a new type.  Any language with generics
has some notion of type constructors.  For example in Scala and Java, `Array` is a type
constructor.  You can't just have a value of type `Array`, you need to provide
it with another type to get `Array[int]` or `Array[String]`.  So we can think
of `Array` as a function (written as `Array[_]`) that accepts one argument and returns a complete type.

In Scala, classes, traits, and type aliases can be type constructors:
```scala
class Foo[T]
trait Bar[A, T <: Foo[A]]
type Baz[T] = Bar[T, Foo[T]]
```
Type theorists often use a star notation to refer to the _kind_ of a type.
Types like `Int`, `Array[Int]`, and `Int => Int` all have kind $\ast$, which
indicates that they're fully constructed, concrete types.  The type constructor
`Array[_]` has kind $\ast \to \ast$, indicating that it must be provided a type to
produce another type.  A type constructor like `Map[_,_]` has kind $\ast \to
\ast \to \ast$.

But that's as far as most type systems go.  Languages like Scala and Haskell
though support types with kinds like $(\ast \to \ast) \to \ast$ or $(\ast \to
(\ast \to \ast)) \to (\ast \to \ast)$.  This allows you to do some really
interesting stuff:

```scala
//kind (* -> *) -> *
type MakeInt[A[_]] = A[Int]

val myArray: MakeInt[Array] = new Array[Int]()

//kind ((* -> *) -> *) -> (* -> *) -> *
type Nested[A[_[_]], B[_]] = A[B]

val myOption: Nested[MakeInt, Option] = Some(4)

//kind (* -> * -> *) -> * -> (* -> *)
type PartialApply[A[_,_], B] = ({type L[T] = A[B,T]})#L

type WhatIsThis = MakeInt[PartialApply[Map, String]]

val myMap: WhatIsThis = Map[String, Int]()
```
You can see here how we are able to use the type constructors `Array` and `Map`
as _parameters_ to the higher-kinded types `MakeInt` and `PartialApply`.
`PartialApply` goes even one step further and returns a _type lambda_, an
anonymous type constructor (sadly the syntax for type lambdas in scala is kind
of bizarre, though to be fair you almost never see them in actual Scala
code, we also will not be using them here).

### Type Members and Type Projection

Just as HKT give us a typelevel analog of higher-order functions, type members
and type projection give us a typelevel analog of properties, methods, and polymorphism.

Normally you use a `trait` to define an abstract interface that can contain
function signatures, but they can also contain abstract type aliases called
_type members_:

```scala
trait Foo {
  type A  //an abstract type member

  def obj: A
}

class StringFoo extends Foo {
  type A = String  //a concrete type member

  def obj: String = "hello"
}

trait Bar extends Foo {
  type A <: String //refinement on an abstract type member
}
```
At first glance, type members may look like another way to add generics or type
parameters to a class or trait, and in fact every place you would use type parameters
you could also use type members.  But another Scala feature known as _type
projection_ makes type members far more useful for us:

```scala
def foobar[F <: Foo](f: F): F#A = f.obj
```
The type projection operator `#` lets us refer to the type members of a type
much in the same way you'd refer to the properties of a case class or a struct.
Using type constructors as type members allows us to write type-level methods:

```scala
trait Foo[T] {
  type MakeAMap[U] = Map[T,U]
}

val bar : Foo[Int]#MakeAMap[String] = Map[Int, String]()
```
Also similarly to their object-oriented counterparts, type members will provide us a degree of typelevel polymorphism:

```scala
trait A[T]
trait B[T] extends A[T]
trait C[T, U] extends A[T]

trait Foo {
  type MakeSomething[T] <: A[T]
}
trait FooChild1 extends Foo {
  type MakeSomething[T] = B[T]
}
trait FooChild2[U] extends Foo {
  type MakeSomething[T] = C[T,U]
}

type Something[F <: Foo] = F#MakeSomething[Int]
```

This is going to help us greatly when it comes to pattern matching on typelevel types.

## Starting Simple : Peano Numbers

Before we get into encoding LC terms as types, we'll start with a simpler
situation which we will need anyway: encoding natural numbers (non-negative
integers) as types.  We will do this by adapting a number system originally
created by the mathematician Giuseppe Peano in the 1800's.  Peano defined the
natural numbers purely in terms of $0$ and a successor function $S(n)$.  So $1
= S(0)$, $2 = S(1) = S(S(0))$, and so-on.  

This is very easy to write in regular Scala as an Algebraic Datatype (ADT):

```scala
sealed trait Nat
case object Zero extends Nat
case class S(n: Nat) extends Nat

val one = S(Zero)
val two = S(one)

//adding two naturals unwraps one while applying the successor to the other
def add(n1: Nat, n2: Nat): Nat = n1 match {
  case Zero => n2
  case S(i) => add(i, S(n2))
}

//equality checking involves recursively unwrapping both numbers and checking
//if they have the same "length"
def equals(n1: Nat, n2: Nat): Boolean = (n1, n2) match {
  case (Zero, Zero) => true
  case (Zero, _) => false
  case (_, Zero) => false
  case (S(i), S(j)) => equals(i,j)
}
```

So how do we create a typelevel ADT?  Actually that's the easy part, we just use traits:

```scala
sealed trait Nat
trait Zero extends Nat
trait S[N <: Nat] extends Nat

type one = S[Zero]
type two = S[one]
type three = S[two]
```

The trait itself has no methods or fields, but that's perfectly fine since we
only care about the types and not creating values of those types.  Also it
should be clear that at the typelevel we're using Scala types both for types
and values, and we will soon further blur this distinction.

### Typelevel addition

But having just the Peano numbers on their own isn't any help, we need to be
able to perform operations on them.  Let's start with addition.  The `add`
function we wrote for the Scala ADT uses pattern matching on one of the numbers
and either immediately returns the other or recursively descends.  So right off
the bat we know we are going to need both pattern matching and some kind of
recursive type constructor.

```scala
sealed trait Nat {
  // define an abstract type-constructor member that accepts a Nat as a
  // parameter and returns a Nat
  type Add[K <: Nat]  <: Nat
}
trait Zero extends Nat {
  //we know that adding a natural to zero is just the number itself
  type Add[K <: Nat] = K
}
trait S[N <: Nat] extends Nat {
  //in this case we know we're not zero, so call add on the nested natural N,
  //and pass S[K] to it as the parameter
  type Add[K <: Nat] = N#Add[S[K]]
}
```
Here we have leveraged the typelevel polymorphism to do the pattern matching for us. Let's walk through a couple examples:
```scala
type sum1 = Zero#Add[S[Zero]]
```
This one is easy, we can clearly see that the `Add` type member of `Zero` just returns whatever is passed to it, so `sum1 = S[Zero]`.
```scala
type sum2 = S[Zero]#Add[S[Zero]]
```
Now we're calling `Add` on the `S` type.  Since this instance has `Zero` as its
type parameter, we can see that it will end up calling `Zero#Add` again, but
this time it will pass `S[K]`, which in this case will be `S[S[Zero]]`.  So
yes, we have calculuated $1 + 1 = 2$ at the typelevel.

We can write a typelevel "test" using Scala's `implicitly` and `=:=` type constructors:

```scala
val a = implicitly[one#Add[two] =:= three]
```
To run the test you just need to compile the code.  If the test fails you'll
get a compile error.
  
### Typelevel Equality

To determine if two naturals are equal, we first need some way to encode a
typelevel boolean value.  Of course Scala has a `Boolean` type, but this isn't
going to help us because `Boolean` has no _compile-time_ notion of true or
false.  You can't define a variable of type `false`, which is exactly what we
need.  So we'll have to make our own:

```scala
sealed trait BOOL
trait TRUE extends BOOL
trait FALSE extends BOOL
```

Now, when we look at the Scala equality method we wrote, we had to pattern
match on both values simultaneously.  Unfortunately the polymorphism-based
approach we took with addition isn't going to fully work here.  There's
actually a few possible approaches to this situation (left as an exercise for
the reader), but the one we'll actually take is a little more complex, but can
be generalized and will be re-used further down.  

First, we're going to add a `IsZero` type member to `Nat`:

```scala
sealed trait Nat {
  type IsZero <: BOOL
  //...
}
trait Zero extends Nat {
  type IsZero = TRUE
}
trait S[N <: Nat] extends Nat {
  type IsZero = FALSE
}
```
Next, we'll define the `Eq` type method and implement it for `Zero`

```scala
sealed trait Nat {
  //...
  type Eq[K <: Nat] <: BOOL
}
trait Zero extends Nat {
  //...
  type Eq[K <: Nat] = K#IsZero
}
```
Defining `Eq` for `Zero` was easy, since all we have to do is return whether
the parameter is also `Zero`.  But defining `Eq` for `S` is going to be
trickier since whether we do the recursive call depends on both naturals being
non-zero.  What we really need is full-fledged pattern matching and
destructuring.  Just as with regular scala pattern matching when we do `case
S(i) => ...` we not only match on the specific subtype but we also get access
to the inner contained value.

We will define pattern matching as a higher-kinded type member on `Nat`:

```scala
sealed trait Nat {
  type Match[IfZero <: T, IfNonZero[N <: Nat] <: T, T] <: T
}
trait Zero extends Nat {
  type Match[IfZero <: T, IfNonZero[N <: Nat] <: T, T] = IfZero
}
trait S[N <: Nat] extends Nat {
  type Match[IfZero <: T, IfNonZero[N <: Nat] <: T, T] = IfNonZero[N]
}
```
Now if we were to do `N#Match` on an arbitary natural, we basically have to
provide the method with what to return in each case, just as we do with regular
pattern matching.  Notice that `IfNonZero` is a type constructor itself and
passes its inner `N` to it, which is how we achieve the destructuring on `S`.
The third parameter `T` acts like a regular type parameter and is just needed
to properly restrict the "return types" of each branch.  This way if each
branch returns a `BOOL` we can ensure the return type of the whole expression
is also `BOOL`.

Using `Match`, we can now implement `Eq` for `S`.  Let's put everything together:

```scala
sealed trait Nat {
  type IsZero <: BOOL

  type Match[IfZero <: T, IfNonZero[I <: Nat] <: T, T] <: T

  type Eq[K <: Nat] <: BOOL

  type Add[K <: Nat] <: Nat

}

trait Zero extends Nat {
  type IsZero = TRUE

  type Match[IfZero <: T, IfNonZero[I <: Nat] <: T, T] = IfZero

  type Eq[K <: Nat] = K#IsZero

  type Add[K <: Nat] = K
}

trait S[N <: Nat] extends Nat {
  type IsZero = FALSE

  type Match[IfZero <: T, IfNonZero[I <: Nat] <: T, T] = IfNonZero[N]

  type Eq[K <: Nat] = K#Match[FALSE, N#Eq, BOOL]

  type Add[K <: Nat] = N#Add[S[K]]
}

```
Notice in `S#Eq` that we're using the new `Match` member and doing the
recursion by passing `N#Eq` as a function in the `IfNonZero` branch.

Let's walk through another example to see how this all expands (I'll be using `one, two` instead of `S[Zero], S[S[Zero]]` for clarity):

```scala
type res = three#Eq[one]
           // calling Eq[K] on three <: S[N] with N=two and K=one yields
         = one#Match[FALSE, two#Eq, BOOL]
           // calling Match on one <: S[N] with N=Zero means 
           // the IfNonZero[N] branch is returned, which in this case returns
         = two#Eq[Zero]
           // calling Eq[K] on two <: S[N] with N=one and K=Zero yields
         = Zero#Match[FALSE, one#Eq, BOOL]
           // calling Match on Zero returns the IfZero parameter, which in this case is
         = FALSE
```
So we can see that type constructors returning other type constructors allows
us to do recursion and traverse through the Peano numbers.


### Outputting Results

So far we've shown how to write typelevel tests like:

```scala
implicitly[S[S[Zero]]#Add[S[Zero]]#Eq[S[S[S[Zero]]]] =:= TRUE]
```

But of course we want to provide a way to calculute and output results that we
don't already know in advance.  To do this we'll actually have the compiler
generate data structures that at run-time will generate a string that can be
output.  We can do this using implicit parameters.

What we want is to be able to do:

```scala
type result = S[S[Zero]]#Add[S[S[S[Zero]]]]

val rep = rep[result]

println(rep) //prints "5" at runtime
```
We can do this by defining a data structure and some implicits.
```scala

def rep[T](implicit r: Rep[T]) = r

sealed trait Rep[T]

sealed trait NatRep[N <: Nat] extends Rep[N] {
  def intValue: Int
  override def toString = intValue.toString
}
case object ZeroRep extends NatRep[Zero] {
  def intValue = 0
}
case class SRep[N <: Nat](n: NatRep[N]) extends NatRep[S[N]] {
  def intValue = 1 + n.intValue
}

implicit def zeroRep: NatRep[Zero] = ZeroRep
implicit def srep[N <: Nat](implicit nrep: NatRep[N]): NatRep[S[N]] = SRep[N](nrep)
```

Now if we were to call `rep[S[S[S[Zero]]]]`, the compiler will actually
generate 

```scala
rep[S[S[S[Zero]]]](srep(srep(srep(zeroRep)))) //returns a SRep(SRep(SRep(ZeroRep)))
```
resulting in exactly what we want:
```
scala> rep[S[S[S[Zero]]]]
res0: Rep[S[S[S[Zero]]]] = 3
```
## Typelevel If Statements

Now let's add some more functionality to our boolean type.  In particular let's implement an `If` type member:

```scala
sealed trait BOOL {
  type If[IfTrue <: T, IfFalse <: T, T] <: T
}
trait TRUE extends BOOL {
  type If[IfTrue <: T, IfFalse <: T, T] = IfTrue
}
trait FALSE extends BOOL {
  type If[IfTrue <: T, IfFalse <: T, T] = IfFalse
}

trait Foo
trait Bar extends Foo
trait Baz extends Foo

//write a test using it
implicitly[two#Eq[three]#If[Bar, Baz, Foo] =:= Baz]
```


## Typelevel Lists

The next component we're going to need is a typelevel list of naturals.  This is actually pretty straightforward:

```scala
sealed trait NatList
class NatNil extends NatList
class NatCons[Item <: Nat, Next <: NatList] extends NatList

```
And at this point you should be able to see how we could def a `Match` method
to implement operations like `Contains[N <: Nat] <: BOOL` and 
`Fold[Start <: Nat, Op[_ <: Nat, _ <: Nat]] <: Nat`.

Because it just feels kind of wrong making a list only for naturals, we can add a type member to make it typelevel generic:

```scala

sealed trait TList{ type T }
class TNil[X] extends TList{type T = X}
class TCons[I, N <: TList{ type T >: I }] extends TList{ type T = N#T }
```
The supertype restriction `T >: I` basically means that every item in the list
must be a subtype of whatever type is in the `TNil`.  Also notice that the
`TCons` propagates the type member of the `TList` it's being prepended to.

However now we're missing something: equality.  So let's create a typelevel interface and require that it be implemented for anything we put in a `TList`:

```scala
sealed trait Eq[T] {
  type Eq[Other <: T] <: BOOL
}

sealed trait TList{ type T <: Eq[T] }
class TNil[X] extends TList{type T = X}
class TCons[I <: Eq[I], N <: TList{ type T >: I }] extends TList{ type T = N#T }

```

```scala

sealed trait TListRep[T <: TList] extends Rep[T] {
  def listString(start: Boolean): String
}
case class TNilRep[T]() extends TListRep[TNil[T]] {
  override def toString = "]"

  def listString(start: Boolean): String = toString
}
case class TConsRep[I, N <: TList{ type T >: I }]
(i: Rep[I], r: TListRep[N]) extends TListRep[TCons[I,N]] {
  override def toString = listString(true)

  def listString(start: Boolean) :String = {
    val begin = if (start) "[" else ","
    s"${begin}${i}${r.listString(false)}"
  }
}

implicit def hlnr[T] = TNilRep[T]()
implicit def hmcr[I, N <: TList{ type T >: I }](implicit item: Rep[I], next: TListRep[N]): TConsRep[I,N] = TConsRep(item, next)
```





## Encoding Lambda-Calculus Terms as Scala Types

In our implementation for Peano natural numbers we introduced all the typelevel
machinery necessary to build our interpreter.  The first thing we need is a
typelevel data structure to represent a lambda calculus expression.  We'll also
add a `Match` method similar to what we did for naturals:

```scala

sealed trait Term {
  type Match[IfVar <: T, IfLambda[_ <: Var, _ <: Term] <: T, IfApply[_ <: Term, _ <: Term] <: T, T] <: T
}

trait Var extends Term {
  type Id <: Nat

  type Match[IfVar <: T, IfLambda[_ <: Var, _ <: Term] <: T, IfApply[_ <: Term, _ <: Term] <: T, T] = IfVar
}


trait Lambda[V <: Var, T <: Term] extends Term {
  type Match[IfVar <: U, IfLambda[_ <: Var, _ <: Term] <: U, IfApply[_ <: Term, _ <: Term] <: U, U] = IfLambda[V,T]
}

trait Apply[A <: Term, B <: Term] extends Term {
  type Match[IfVar <: T, IfLambda[_ <: Var, _ <: Term] <: T, IfApply[_ <: Term, _ <: Term] <: T, T] = IfApply[T1, T2]
}

//define an alias to make constructing variables easier
type MV[N <: Nat] = Var { Id = N }

//define some variables
type X = MV[Zero]
type Y = MV[S[X#Id]]
type Z = MV[S[Y#Id]]

//define an expression
type my_expression = Apply[Lambda[X, X], Y]
```

Looks like a good start, we've defined a type that essentially encodes the
expression $((\lambda x ~ x) ~ y)$

### Typelevel If

The first thing we'll tackle is variable equality, since we already said we're
going to need it, and our approach to variable equality will resemble how we're
going to build the rest of the interpreter.  So what we need is a type
constructor that accepts two types as input and returns a type that somehow
represents a boolean.

First, we'll define some types for our booleans

```scala
sealed trait BOOL
trait TRUE extends BOOL
trait FALSE extends BOOL
```

Next, we'll add two type members to our `Var` type.  The first will be a simple `isNil <: BOOL` that we'll hardcode to `TRUE` and `FALSE` in the two subtypes.

```scala
trait Var extends Term{
  type IsNil <: BOOL
}
trait VNil extends Var {
  type IsNil = TRUE
}

type X = VNil
type is_x_nil = X#IsNil //TRUE

type Y = V[X]
type is_y_nil = Y#IsNil //FALSE
```

Now let's think about how we would write an equality check based on checking if a `Var` is `VNil`.  If we were writing normal scala and `Var` was a regular ADT (like a list), we would write something like

```scala
def equal(x: Var, y: Var): Boolean = (x,y) match {
  case (VNil, VNil) => true
  case (VNil, _) => false
  case (_, VNil) => false
  case (V(i), V(j)) => equal(i,j)
}
```
This is of course the same kind of algorithm to check if two linked lists have the same length.
